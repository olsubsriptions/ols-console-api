package pe.olympus.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import pe.olympus.repository.UserRepository;

public class TokenFilter extends OncePerRequestFilter  {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private FirebaseApp firebaseApp;
	@Autowired
	private UserRepository userRepository;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
				
		String idToken = getTokenFromRequest(request);
		FirebaseToken decodedToken = null;
		try {
			
			if(request.getRequestURI().equals("/v")) {
				response.sendRedirect(userRepository.getEmailValidationLink(request.getParameter("cid")));				
			} else {
				if(idToken != null) {
					decodedToken = FirebaseAuth.getInstance(firebaseApp).verifyIdToken(idToken);
				}
				if (decodedToken != null) {			
					User user = new User();
					user.setUid(decodedToken.getUid());
					user.setName(decodedToken.getName());
					user.setEmail(decodedToken.getEmail());
					user.setPicture(decodedToken.getPicture());
					user.setIssuer(decodedToken.getIssuer());
					user.setEmailVerified(decodedToken.isEmailVerified());
					UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
							user, decodedToken, null);
					authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
				filterChain.doFilter(request, response);
			}	
			
		} catch (FirebaseAuthException e) {
			logger.error("Firebase #error : {}", e.getMessage());
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid Authorization header.");
		}
	}
	
	public String getTokenFromRequest(HttpServletRequest request) {
		String token = null;
		
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			token = bearerToken.substring(7, bearerToken.length());
		}
		
		return token;
	}
    
}
