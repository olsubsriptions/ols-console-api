package pe.olympus.constants;

public class ApplicationCode {
	
	public static final String MONETA = "MNT";
	public static final String SECURITY_PROTECCION_SUITE = "SPS";
	public static final String TRANSLINK_TRANSACTION_SERVICES = "TTS";
	
}
