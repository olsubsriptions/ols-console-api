package pe.olympus.constants;

public class ResponseMessages {
	public static final String OPERACION_EXITOSA = "La operación se ha realizado exitosamente";
	public static final String OPERACION_NOEXITOSA = "La operación no se ha completado";
	public static final String SUSCRIPCION_VALIDA = "Suscripción Válida";
	public static final String SUSCRIPCION_INVALIDA = "Suscripción Inválida";
	public static final String APLICACION_NOEXISTE = "La Aplicación no existe";
	public static final String SUSCRIPCION_EXISTE = "Ya cuenta con una Suscripción para esta Aplicación";
	public static final String CIPHER_STRUCTURE_ERROR = "La estructura del cipher no es correcta";
	public static final String EMAIL_VERIFIED = "Email Verificado";
	public static final String EMAIL_NOTVERIFIED = "Email No Verificado";
	public static final String USER_NOTFOUND = "El Usuario no existe";
	
}
