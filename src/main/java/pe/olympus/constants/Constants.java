package pe.olympus.constants;

public class Constants {
	
	public static final String T_CONFIG = "cnfg";
	public static final String T_USERS = "g6730";
	public static final String T_SUBSCRIPTIONS = "b4320";
	public static final String T_ORGANIZATIONS = "f4460";
	public static final String T_APPLICATIONS = "k0870";
	public static final String T_PROFILE = "u4320";
	public static final String T_PROFILE_ELEMENTS = "x2810";
	public static final String T_APPLICATION_BY_SUBSCRIPTION = "w0790";
	public static final String T_COMPONENTS = "c7050";
	public static final String T_SUBSCRIPTION_COMPONENTS = "y9070";
	public static final String T_INSTANCES = "i7070";
	public static final String T_SUBSCRIPTION_INSTANCES = "d0730";
	public static final String KEY_K1 = "K1";
	public static final String KEY_K2 = "K2";
	public static final String KEY_SK = "SK";
	public static final String ALGORITHM_AES256 = "AES256";
	public static final String CHARSET_UTF8 = "UTF-8";

}
