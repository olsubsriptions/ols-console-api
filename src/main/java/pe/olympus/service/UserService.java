package pe.olympus.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.UserDTO;
import pe.olympus.model.UserS;
import pe.olympus.repository.UserRepository;

@Service
public class UserService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private UserRepository userRepository;
	
	@Autowired
	public UserService (UserRepository userRepository) {
		
		this.userRepository = userRepository;
	}
	
	public List<UserS> getUsers (String userId) {
		
		List<UserS> listUsers = null;
		UserS user = userRepository.getUserById(userId);
		if(user != null) {
			listUsers = userRepository.getUsersBySubscriptionId(user.getB4321());
		}
		logger.info("UserService.getUsers : getUsers() is done");
		return listUsers;
	}
	
//	public void emailValidation (String cid) {
//		userRepository.emailValidation(cid);
//		logger.info("UserService.emailValidation : emailValidation() is done");
//	}
	
	public GenericResponse loginValidation(UserDTO userDto) {
		GenericResponse genericResponse = userRepository.loginValidation(userDto);
		logger.info("UserService.loginValidation : loginValidation() is done");
		return genericResponse;
	}
	
	public UserS getUserInfo(UserDTO userDto) {
		UserS user = userRepository.getUserInfo(userDto);
		logger.info("UserService.getUserInfo : getUserInfo() is done");
		return user;
	}
	
	public GenericResponse userUnlock(UserDTO userDto) {
		GenericResponse genericResponse = userRepository.userUnlock(userDto);
		logger.info("UserService.userUnlock : userUnlock() is done");
		return genericResponse;
	}
	
	public GenericResponse userChangePassword(UserDTO userDto) {
		GenericResponse genericResponse = userRepository.userChangePassword(userDto);
		logger.info("UserService.userChangePassword : userChangePassword() is done");
		return genericResponse;
	}

}
