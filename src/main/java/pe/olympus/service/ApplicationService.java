package pe.olympus.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.olympus.model.AppComponent;
import pe.olympus.model.AppInstance;
import pe.olympus.model.Application;
import pe.olympus.model.ApplicationDTO;
import pe.olympus.model.GenericResponse;
import pe.olympus.repository.ApplicationRepository;

@Service
public class ApplicationService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private ApplicationRepository applicationRepository;
	
	@Autowired
	public ApplicationService(ApplicationRepository applicationRepository) {
		
		this.applicationRepository = applicationRepository;
	}

	public List<Application> getApplications(String userId){		
		
		List<Application> listApplication = applicationRepository.getApplications(userId);
		logger.info("ApplicationService.getApplications : getApplications() is done");
		return listApplication;		
	}
	
	public List<ApplicationDTO> getAllApplications(){
		
		List<ApplicationDTO> listApplication = applicationRepository.getAllApplications();
		logger.info("ApplicationService.getAllApplications : getAllApplications() is done");
		return listApplication;
	}
	
	public GenericResponse createApplication(Application application){
		
		GenericResponse genericResponse = applicationRepository.createApplication(application);
		logger.info("ApplicationService.createApplication : createApplication() is done");
		return genericResponse;
	}
	
	public GenericResponse createInstance(AppInstance appInstance) {
		
		GenericResponse genericResponse = applicationRepository.createInstance(appInstance);
		logger.info("ApplicationService.createInstance : createInstance() is done");
		return genericResponse;
	}
	
	public GenericResponse updateInstance(AppInstance appInstance) {
		
		GenericResponse genericResponse = applicationRepository.updateInstance(appInstance);
		logger.info("ApplicationService.updateInstance : updateInstance() is done");
		return genericResponse;
	}
	
	public List<AppComponent> getAppComponentsByAppSubscriptionId(String appSubscriptionId, String appShortName){
		
		List<AppComponent> listAppComponent = applicationRepository.getAppComponentsByAppSubscriptionId(appSubscriptionId, appShortName);
		logger.info("ApplicationService.getAppComponentsByApplicationId : getAppComponentsByApplicationId() is done");
		return listAppComponent;
	}
}
