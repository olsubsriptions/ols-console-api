package pe.olympus.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import pe.olympus.model.CipherRequest;
import pe.olympus.model.GeneratedKey;
import pe.olympus.model.ResponseObject;
import pe.olympus.model.SubsConfig;
import pe.olympus.model.Subscription;
import pe.olympus.model.UserS;
import pe.olympus.repository.SubscriptionRepository;
import pe.olympus.repository.UserRepository;

@Service
public class SubscriptionService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private SubscriptionRepository subscriptionRepository;
	private UserRepository userRepository;
	private CryptoService cryptoService;
	private Gson gsonTemplate;
	
	@Autowired
	public SubscriptionService(SubscriptionRepository subscriptionRepository,
			UserRepository userRepository,
			CryptoService cryptoService,
			Gson gsonTemplate) {
		
		this.subscriptionRepository = subscriptionRepository;
		this.userRepository = userRepository;
		this.cryptoService = cryptoService;
		this.gsonTemplate = gsonTemplate;
	}
	
	public Subscription getSubscriptionByUserId (String userId){
		
		Subscription subscription = null;
		UserS user = userRepository.getUserById(userId);		
		if(user != null) {
			subscription = subscriptionRepository.getSubscriptionById(user.getB4321());
		}
		
		logger.info("SubscriptionService.getSubscriptionByUserId : getSubscriptionByUserId() is done");
		return subscription;
	}
	
	public ResponseObject getK1 (String userId) {
		
		ResponseObject response = null;
		UserS user = userRepository.getUserById(userId);
		if(user != null) {
			Subscription subscription = subscriptionRepository.getSubscriptionById(user.getB4321());
			if(subscription != null) {
				SubsConfig configuration = subscriptionRepository.getConfiguration();				
				if(configuration != null) {					
					response = generateK1(subscription, configuration);					
				}				
			}
		}
		
		logger.info("SubscriptionService.getK1 : getK1() is done");
		return response;
	}	

	public ResponseObject regenerateK1 (String userId) {
		
		ResponseObject response = null;
		UserS user = userRepository.getUserById(userId);		
		if(user != null) {
			String derivativeKey = cryptoService.getKey();
			Subscription subscription = subscriptionRepository.updateSubscriptionDerivativeKey(user.getB4321(), derivativeKey);
			if(subscription != null) {
				SubsConfig configuration = subscriptionRepository.getConfiguration();
				if(configuration != null) {					
					response = generateK1(subscription, configuration);
				}				
			}			
		}		
		
		logger.info("SubscriptionService.regenerateK1 : regenerateK1() is done");
		return response;
	}
	
	private ResponseObject generateK1(Subscription subscription, SubsConfig configuration) {
		
		GeneratedKey k1 = new GeneratedKey();
		k1.setObjectId(subscription.getB4321());
		k1.setDerivativeKey(subscription.getB4324());
		k1.setEndPoint(configuration.getSubscriptionEndPoint());
		k1.setPort(configuration.getServicePort());
		
		CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(gsonTemplate.toJson(k1), null);
		
		ResponseObject response = cryptoService.encrypt(cipherRequest);
		if(response.getDataList().size() > 0) {
			response.setEncKCnfgData(response.getDataList().get(0));
		}
		
		logger.info("SubscriptionService.generateK1 : generateK1() is done");
		return response;
	}
}
