package pe.olympus.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import pe.olympus.model.Organization;
import pe.olympus.model.UserS;
import pe.olympus.repository.OrganizationRepository;
import pe.olympus.repository.UserRepository;

@Service
public class OrganizationService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private UserRepository userRepository;
	private OrganizationRepository organizationRepository;
	
	@Autowired
	public OrganizationService(UserRepository userRepository,
			OrganizationRepository organizationRepository) {
		
		this.userRepository = userRepository;
		this.organizationRepository = organizationRepository;
	}
	
	public Organization getOrganizationByUserId(String userId) {
		
		Organization organization = null;
		UserS user = userRepository.getUserById(userId);		
		if(user != null) {
			organization = organizationRepository.getOrganizationBySubscriptionId(user.getB4321());
			organization.setB4321(user.getB4321());
		}
		
		logger.info("OrganizationService.getOrganizationByUserId : getOrganizationByUserId() is done");
		return organization;
	}
}
