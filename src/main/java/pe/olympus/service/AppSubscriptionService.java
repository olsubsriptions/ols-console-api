package pe.olympus.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import pe.olympus.model.BindDTO;
import pe.olympus.model.AgentDTO;
import pe.olympus.model.AppSubsComponent;
import pe.olympus.model.AppSubscription;
import pe.olympus.model.CipherRequest;
import pe.olympus.model.GeneratedKey;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.ResponseObject;
import pe.olympus.model.UserS;
import pe.olympus.model.VerifyResponse;
import pe.olympus.repository.AppSubscriptionRepository;
import pe.olympus.repository.UserRepository;

@Service
public class AppSubscriptionService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private UserRepository userRepository;
	private AppSubscriptionRepository appSubscriptionRepository;
	private CryptoService cryptoService;
	private Gson gsonTemplate;
	
	@Autowired
	public AppSubscriptionService(UserRepository userRepository,
			AppSubscriptionRepository appSubscriptionRepository,
			CryptoService cryptoService,
			Gson gsonTemplate) {
		
		this.userRepository = userRepository;
		this.appSubscriptionRepository = appSubscriptionRepository;
		this.cryptoService = cryptoService;
		this.gsonTemplate = gsonTemplate;
	}

	public List<AppSubscription> getAppsSubscriptionByUserId(String userId) {
		
		List<AppSubscription> listAppSubscription = null;
		UserS user = userRepository.getUserById(userId);		
		if(user != null) {
			listAppSubscription = appSubscriptionRepository.getAppsSubscriptionBySubscriptionId(user.getB4321());
		}
		
		logger.info("AppSubscriptionService.getAppsSubscriptionByUserId : getAppsSubscriptionByUserId() is done");
		return listAppSubscription;
	}
	
	public AppSubscription getAppSubscription (String userId, String applicationId) {
		
		AppSubscription appSubscription = null;
		UserS user = userRepository.getUserById(userId);
		if(user != null) {
			appSubscription = appSubscriptionRepository.getAppSubscription(user.getB4321(), applicationId);			
		}
		
		logger.info("AppSubscriptionService.getAppSubscription : getAppSubscription() is done");
		return appSubscription;
	}
	
	public AppSubscription getAppSubscriptionById (String appSubscriptionId) {
		
		AppSubscription appSubscription = appSubscriptionRepository.getAppSubscriptionById(appSubscriptionId);
		
		logger.info("AppSubscriptionService.getAppSubscriptionById : getAppSubscriptionById() is done");
		return appSubscription;
	}
	
	public ResponseObject getK2(String appSubscriptionId) {
		
		ResponseObject response = null;		
		AppSubscription appSubscription = appSubscriptionRepository.getAppSubscriptionById(appSubscriptionId);
		
		if(appSubscription != null) {
			
//			AppInstance appInstance = appSubscriptionRepository.getAppInstanceByAppSubscriptionId(appSubscription.getW0791());
//			
//			if(appInstance != null) {
				
				response = generateK2(appSubscription);
//			}			
		}
		
		logger.info("AppSubscriptionService.getK2 : getK2() is done");
		return response;
	}
	
	public ResponseObject regenerateK2(String appSubscriptionId) {
		
		ResponseObject response = null;		
		String derivativeKey = cryptoService.getKey();
		AppSubscription appSubscription = appSubscriptionRepository.updateAppSubscriptionDerivativeKey(appSubscriptionId, derivativeKey);
		
		if(appSubscription != null) {
			
//			AppInstance appInstance = appSubscriptionRepository.getAppInstanceByAppSubscriptionId(appSubscription.getW0791());
//			
//			if(appInstance != null) {
				
				response = generateK2(appSubscription);				
//			}			
		}			
		
		logger.info("AppSubscriptionService.regenerateK2 : regenerateK2() is done");
		return response;
	}
	
	public ResponseObject generateK2(AppSubscription appSubscription) {
		
		GeneratedKey k2 = new GeneratedKey();
		k2.setObjectId(appSubscription.getW0791());
		k2.setDerivativeKey(appSubscription.getW0796());
		k2.setEndPoint(appSubscription.getW0795());
		k2.setPort(appSubscription.getW0798());
		
		CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(gsonTemplate.toJson(k2), null);		
		
		ResponseObject response = cryptoService.encrypt(cipherRequest);
		if(response.getDataList().size() > 0) {
			response.setEncKCnfgData(response.getDataList().get(0));
		}
		
		logger.info("AppSubscriptionService.generateK2 : generateK2() is done");
		return response;
	}
	
	public VerifyResponse verifySubscriptionByIndexKey (String indexKey, String workingApplicationKey) {
		
		VerifyResponse verifyResponse = appSubscriptionRepository.verifySubscriptionByIndexKey(indexKey, workingApplicationKey);
		
		logger.info("AppSubscriptionService.verifySubscriptionByIndexKey : verifySubscriptionByIndexKey() is done");
		return verifyResponse;
	}
	
	public GenericResponse createAppSubscription (AppSubscription appSubscription){	
		
		GenericResponse genericResponse = appSubscriptionRepository.createAppSubscription(appSubscription);
		
		logger.info("AppSubscriptionService.createAppSubscription : createAppSubscription() is done");
		return genericResponse;
	}
	
	public GenericResponse createAppSubsComponents(List<AppSubsComponent> listAppSubsComponents) {
		
		GenericResponse genericResponse = appSubscriptionRepository.createAppSubsComponents(listAppSubsComponents);
		
		logger.info("AppSubscriptionService.createAppSubsComponents : createAppSubsComponents() is done");
		return genericResponse;
	}
	
	public VerifyResponse verifySubscription (BindDTO bindDTO) {
		
		VerifyResponse verifyResponse = appSubscriptionRepository.verifySubscription(bindDTO);
		
		logger.info("AppSubscriptionService.verifySubscription : verifySubscription() is done");
		return verifyResponse;
	}
	
	public GenericResponse saveApplicationData (AgentDTO agentDTO) {
		
		GenericResponse genericResponse = appSubscriptionRepository.saveApplicationData(agentDTO);
		
		logger.info("AppSubscriptionService.saveApplicationData : saveApplicationData() is done");
		return genericResponse;
	}
}
