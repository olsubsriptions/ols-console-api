package pe.olympus.model;

import java.util.ArrayList;
import java.util.List;

public class ApplicationDTO {

	private String k0871; // ouid
	private String u4321; // profileID
	private String k0872; // applicationName
	private String k0873; // applicationShortName
	private String k0874; // version
	private String k0875; // versionMajor
	private String k0876; // versionMinor
	private String k0877; // applicationEndpoint
	private String k0878; // port
	private boolean k0879; // state
	private String k0880; // image
	private List<AppInstance> instances = new ArrayList<AppInstance>();
	private List<AppComponent> components = new ArrayList<AppComponent>();
	
	public String getK0871() {
		return k0871;
	}
	public void setK0871(String k0871) {
		this.k0871 = k0871;
	}
	public String getU4321() {
		return u4321;
	}
	public void setU4321(String u4321) {
		this.u4321 = u4321;
	}
	public String getK0872() {
		return k0872;
	}
	public void setK0872(String k0872) {
		this.k0872 = k0872;
	}
	public String getK0873() {
		return k0873;
	}
	public void setK0873(String k0873) {
		this.k0873 = k0873;
	}
	public String getK0874() {
		return k0874;
	}
	public void setK0874(String k0874) {
		this.k0874 = k0874;
	}
	public String getK0875() {
		return k0875;
	}
	public void setK0875(String k0875) {
		this.k0875 = k0875;
	}
	public String getK0876() {
		return k0876;
	}
	public void setK0876(String k0876) {
		this.k0876 = k0876;
	}
	public String getK0877() {
		return k0877;
	}
	public void setK0877(String k0877) {
		this.k0877 = k0877;
	}
	public String getK0878() {
		return k0878;
	}
	public void setK0878(String k0878) {
		this.k0878 = k0878;
	}
	public boolean isK0879() {
		return k0879;
	}
	public void setK0879(boolean k0879) {
		this.k0879 = k0879;
	}
	public String getK0880() {
		return k0880;
	}
	public void setK0880(String k0880) {
		this.k0880 = k0880;
	}
	public List<AppInstance> getInstances() {
		return instances;
	}
	public void setInstances(List<AppInstance> instances) {
		this.instances = instances;
	}
	public List<AppComponent> getComponents() {
		return components;
	}
	public void setComponents(List<AppComponent> components) {
		this.components = components;
	}
	@Override
	public String toString() {
		return "ApplicationDTO [k0871=" + k0871 + ", u4321=" + u4321 + ", k0872=" + k0872 + ", k0873=" + k0873
				+ ", k0874=" + k0874 + ", k0875=" + k0875 + ", k0876=" + k0876 + ", k0877=" + k0877 + ", k0878=" + k0878
				+ ", k0879=" + k0879 + ", k0880=" + k0880 + ", instances=" + instances + ", components=" + components
				+ "]";
	}	
}
