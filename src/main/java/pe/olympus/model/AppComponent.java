package pe.olympus.model;

import java.util.List;

public class AppComponent {
	
	private String c7051; // ouid
	private String k0871; // applicationId
	private String c7052; // componentName
	private boolean c7053; // state
	private int c7054; // componetType
	private List<AppSubsComponent> subsComponents; // list AppSubsComponent
	
	public String getC7051() {
		return c7051;
	}
	public void setC7051(String c7051) {
		this.c7051 = c7051;
	}
	public String getK0871() {
		return k0871;
	}
	public void setK0871(String k0871) {
		this.k0871 = k0871;
	}
	public String getC7052() {
		return c7052;
	}
	public void setC7052(String c7052) {
		this.c7052 = c7052;
	}
	public boolean isC7053() {
		return c7053;
	}
	public void setC7053(boolean c7053) {
		this.c7053 = c7053;
	}	
	public int getC7054() {
		return c7054;
	}
	public void setC7054(int c7054) {
		this.c7054 = c7054;
	}	
	public List<AppSubsComponent> getSubsComponents() {
		return subsComponents;
	}
	public void setSubsComponents(List<AppSubsComponent> subsComponents) {
		this.subsComponents = subsComponents;
	}
	@Override
	public String toString() {
		return "AppComponent [c7051=" + c7051 + ", k0871=" + k0871 + ", c7052=" + c7052 + ", c7053=" + c7053
				+ ", c7054=" + c7054 + ", subsComponents=" + subsComponents + "]";
	}
}
