package pe.olympus.model;

public class AppSubsComponent {
	
	private String y9071; // OUID
	private String w0791; // ApplicationSusbcriptionId
	private String c7051; // ApplicationComponentId
	private long y9072; // Ordinal
	private String y9073; // PropertyName
	private String y9074; // ShortName
	private String y9075; // DefaultPropertyValue
	private boolean y9076; // State
	
	public String getY9071() {
		return y9071;
	}
	public void setY9071(String y9071) {
		this.y9071 = y9071;
	}
	public String getW0791() {
		return w0791;
	}
	public void setW0791(String w0791) {
		this.w0791 = w0791;
	}
	public String getC7051() {
		return c7051;
	}
	public void setC7051(String c7051) {
		this.c7051 = c7051;
	}
	public long getY9072() {
		return y9072;
	}
	public void setY9072(long y9072) {
		this.y9072 = y9072;
	}
	public String getY9073() {
		return y9073;
	}
	public void setY9073(String y9073) {
		this.y9073 = y9073;
	}
	public String getY9074() {
		return y9074;
	}
	public void setY9074(String y9074) {
		this.y9074 = y9074;
	}
	public String getY9075() {
		return y9075;
	}
	public void setY9075(String y9075) {
		this.y9075 = y9075;
	}
	public boolean getY9076() {
		return y9076;
	}
	public void setY9076(boolean y9076) {
		this.y9076 = y9076;
	}
	@Override
	public String toString() {
		return "AppSubsComponent [y9071=" + y9071 + ", w0791=" + w0791 + ", c7051=" + c7051 + ", y9072=" + y9072
				+ ", y9073=" + y9073 + ", y9074=" + y9074 + ", y9075=" + y9075 + ", y9076=" + y9076 + "]";
	}
}
