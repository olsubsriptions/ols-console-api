package pe.olympus.model;

public class AgentDTO {
	
	private String index_key;
	private String transaction_id;
	private String cipher;
	private String working_application_key;	
	
	public String getIndex_key() {
		return index_key;
	}
	public void setIndex_key(String index_key) {
		this.index_key = index_key;
	}
	public String getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}
	public String getCipher() {
		return cipher;
	}
	public void setCipher(String cipher) {
		this.cipher = cipher;
	}
	public String getWorking_application_key() {
		return working_application_key;
	}
	public void setWorking_application_key(String working_application_key) {
		this.working_application_key = working_application_key;
	}
	
	
}
