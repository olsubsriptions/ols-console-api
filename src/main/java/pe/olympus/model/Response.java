package pe.olympus.model;

import org.springframework.http.HttpStatus;

public class Response<T> {
	
	private T body;
	private HttpStatus status;
	private String message;
	
	public Response(T body, HttpStatus status, String message) {
		this.body = body;
		this.status = status;
		this.message = message;
	}
	
	public T getBody() {
		return body;
	}
	public void setBody(T body) {
		this.body = body;
	}
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "Response [body=" + body + ", status=" + status + ", message=" + message + "]";
	}	
}
