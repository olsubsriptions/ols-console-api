package pe.olympus.model;

public class BindDataDTO {
    private String working_subscription_derived_key;
    private String subscription_id;
    private String working_application_derived_key;
    private String application_id;
    
	public String getWorking_subscription_derived_key() {
		return working_subscription_derived_key;
	}
	public void setWorking_subscription_derived_key(String working_subscription_derived_key) {
		this.working_subscription_derived_key = working_subscription_derived_key;
	}
	public String getSubscription_id() {
		return subscription_id;
	}
	public void setSubscription_id(String subscription_id) {
		this.subscription_id = subscription_id;
	}
	public String getWorking_application_derived_key() {
		return working_application_derived_key;
	}
	public void setWorking_application_derived_key(String working_application_derived_key) {
		this.working_application_derived_key = working_application_derived_key;
	}
	public String getApplication_id() {
		return application_id;
	}
	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}
	@Override
	public String toString() {
		return "BindDataDTO [working_subscription_derived_key=" + working_subscription_derived_key
				+ ", subscription_id=" + subscription_id + ", working_application_derived_key="
				+ working_application_derived_key + ", application_id=" + application_id + "]";
	}
}
