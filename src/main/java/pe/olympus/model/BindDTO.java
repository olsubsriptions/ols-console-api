package pe.olympus.model;

public class BindDTO {

	private String index_key;
	private String data;
	public String getIndex_key() {
		return index_key;
	}
	public void setIndex_key(String index_key) {
		this.index_key = index_key;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "AgentDTO [index_key=" + index_key + ", data=" + data + "]";
	}
	
}
