package pe.olympus.model;

public class AgentCipherDTO {
	
	private String workingDomain;
	private String baseDN;
	private String directoryServer;
	private String host;
	private String port;
	private String accountOperator;
	private String password;
	private String status;
	private boolean currentState;
	
	public String getWorkingDomain() {
		return workingDomain;
	}
	public void setWorkingDomain(String workingDomain) {
		this.workingDomain = workingDomain;
	}
	public String getBaseDN() {
		return baseDN;
	}
	public void setBaseDN(String baseDN) {
		this.baseDN = baseDN;
	}
	public String getDirectoryServer() {
		return directoryServer;
	}
	public void setDirectoryServer(String directoryServer) {
		this.directoryServer = directoryServer;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getAccountOperator() {
		return accountOperator;
	}
	public void setAccountOperator(String accountOperator) {
		this.accountOperator = accountOperator;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean getCurrentState() {
		return currentState;
	}
	public void setCurrentState(boolean currentState) {
		this.currentState = currentState;
	}
	@Override
	public String toString() {
		return "AgentCipherDTO [workingDomain=" + workingDomain + ", baseDN=" + baseDN + ", directoryServer="
				+ directoryServer + ", host=" + host + ", port=" + port + ", accountOperator=" + accountOperator
				+ ", password=" + password + ", status=" + status + ", currentState=" + currentState + "]";
	}
	
	public boolean isNull() {
		if(this.workingDomain == null) return true;
		if(this.baseDN == null) return true;
		if(this.directoryServer == null) return true;
		if(this.host == null) return true;
		if(this.port == null) return true;
		if(this.accountOperator == null) return true;
		if(this.password == null) return true;
		if(this.status == null) return true;
		return false;
	}
}
