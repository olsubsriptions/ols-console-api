package pe.olympus.model;

public class GeneratedKey {
	
	private String objectId;
	private String derivativeKey;
	private String endPoint;
	private String port;
	
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getDerivativeKey() {
		return derivativeKey;
	}
	public void setDerivativeKey(String derivativeKey) {
		this.derivativeKey = derivativeKey;
	}
	public String getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	@Override
	public String toString() {
		return "Key [objectId=" + objectId + ", derivativeKey=" + derivativeKey + ", endPoint=" + endPoint + ", port="
				+ port + "]";
	}
	
}
