package pe.olympus.model;

import java.util.List;

public class Crypto {
	
	private String keyType;
	private String objectId;
	private String derivativeKey;
	private String encKey;
	private String encKCnfgData;
	private String securityKey;
	private List<String> dataList;
	private CipherParamRequest cipher;
	
	public String getKeyType() {
		return keyType;
	}
	public void setKeyType(String keyType) {
		this.keyType = keyType;
	}
	public String getObjectId() {
		return objectId;
	}
	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}
	public String getDerivativeKey() {
		return derivativeKey;
	}
	public void setDerivativeKey(String derivativeKey) {
		this.derivativeKey = derivativeKey;
	}
	public String getEncKey() {
		return encKey;
	}
	public void setEncKey(String encKey) {
		this.encKey = encKey;
	}
	public String getEncKCnfgData() {
		return encKCnfgData;
	}
	public void setEncKCnfgData(String encKCnfgData) {
		this.encKCnfgData = encKCnfgData;
	}
	public String getSecurityKey() {
		return securityKey;
	}
	public void setSecurityKey(String securityKey) {
		this.securityKey = securityKey;
	}
	public List<String> getDataList() {
		return dataList;
	}
	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}
	public CipherParamRequest getCipher() {
		return cipher;
	}
	public void setCipher(CipherParamRequest cipher) {
		this.cipher = cipher;
	}
	@Override
	public String toString() {
		return "Crypto [keyType=" + keyType + ", objectId=" + objectId + ", derivativeKey=" + derivativeKey
				+ ", encKey=" + encKey + ", encKCnfgData=" + encKCnfgData + ", securityKey=" + securityKey
				+ ", dataList=" + dataList + ", cipher=" + cipher + "]";
	}
}
