package pe.olympus.model;

import java.util.List;

public class ResponseObject {
	
	private List<String> dataList;
	private String encKCnfgData;
	
	public List<String> getDataList() {
		return dataList;
	}
	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}
	public String getEncKCnfgData() {
		return encKCnfgData;
	}
	public void setEncKCnfgData(String encKCnfgData) {
		this.encKCnfgData = encKCnfgData;
	}
	@Override
	public String toString() {
		return "ResponseObject [dataList=" + dataList + ", encKCnfgData=" + encKCnfgData + "]";
	}	
	
}
