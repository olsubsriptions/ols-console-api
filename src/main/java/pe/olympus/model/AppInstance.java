package pe.olympus.model;

public class AppInstance {
	private String i7071; // ouid
	private String k0871; // ApplicationId
	private String i7072; // InstanceEndPoint
	private String i7073; // InstancePort
	private Boolean i7074; // State
	private int i7075; // Clients
	private String i7076; // InstanceDbUser
	private String i7077; // InstanceDbPass
	// aditional data
	private String d0732; // Nombre de la Cola se graba en la tabla D0730
	
	public String getI7071() {
		return i7071;
	}
	public void setI7071(String i7071) {
		this.i7071 = i7071;
	}
	public String getK0871() {
		return k0871;
	}
	public void setK0871(String k0871) {
		this.k0871 = k0871;
	}
	public String getI7072() {
		return i7072;
	}
	public void setI7072(String i7072) {
		this.i7072 = i7072;
	}
	public String getI7073() {
		return i7073;
	}
	public void setI7073(String i7073) {
		this.i7073 = i7073;
	}
	public Boolean getI7074() {
		return i7074;
	}
	public void setI7074(Boolean i7074) {
		this.i7074 = i7074;
	}
	public int getI7075() {
		return i7075;
	}
	public void setI7075(int i7075) {
		this.i7075 = i7075;
	}
	public String getI7076() {
		return i7076;
	}
	public void setI7076(String i7076) {
		this.i7076 = i7076;
	}
	public String getI7077() {
		return i7077;
	}
	public void setI7077(String i7077) {
		this.i7077 = i7077;
	}	
	public String getD0732() {
		return d0732;
	}
	public void setD0732(String d0732) {
		this.d0732 = d0732;
	}
	@Override
	public String toString() {
		return "AppInstance [i7071=" + i7071 + ", k0871=" + k0871 + ", i7072=" + i7072 + ", i7073=" + i7073 + ", i7074="
				+ i7074 + ", i7075=" + i7075 + ", i7076=" + i7076 + ", i7077=" + i7077 + "]";
	}	
}
