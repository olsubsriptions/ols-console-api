package pe.olympus.model;

public class SubsConfig {
	
	private String pksys;
	private String servicePort;
	private String subscriptionEndPoint;
	private String tpksys;
	
	public String getPksys() {
		return pksys;
	}
	public void setPksys(String pksys) {
		this.pksys = pksys;
	}
	public String getServicePort() {
		return servicePort;
	}
	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}
	public String getSubscriptionEndPoint() {
		return subscriptionEndPoint;
	}
	public void setSubscriptionEndPoint(String subscriptionEndPoint) {
		this.subscriptionEndPoint = subscriptionEndPoint;
	}
	public String getTpksys() {
		return tpksys;
	}
	public void setTpksys(String tpksys) {
		this.tpksys = tpksys;
	}
	
	@Override
	public String toString() {
		return "SubsConfig [pksys=" + pksys + ", servicePort=" + servicePort + ", subscriptionEndPoint="
				+ subscriptionEndPoint + ", tpksys=" + tpksys + "]";
	}	
}
