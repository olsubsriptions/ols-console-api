package pe.olympus.model;

public class CipherParamRequest {
	
	private String algorithm;
	private String charset;
	
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}	
	@Override
	public String toString() {
		return "CipherParamRequest [algorithm=" + algorithm + ", charset=" + charset + "]";
	}	
}
