package pe.olympus.model;

import java.util.List;

public class CipherRequest {
	
	private List<String> dataList;
	private String indexKey;	
	private CipherParamRequest cipher;
	
	public CipherParamRequest getCipher() {
		return cipher;
	}
	public void setCipher(CipherParamRequest cipher) {
		this.cipher = cipher;
	}
	public List<String> getDataList() {
		return dataList;
	}
	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}
	public String getIndexKey() {
		return indexKey;
	}
	public void setIndexKey(String indexKey) {
		this.indexKey = indexKey;
	}

	@Override
	public String toString() {
		return "CipherRequest [dataList=" + dataList + ", indexKey=" + indexKey + ", cipher=" + cipher + "]";
	}

}
