package pe.olympus.model;

public class AppSubscription {
	
	private String w0791; // applicationBySubscriptionId
	private String b4321; // subscriptionId
	private String k0871; // applicationId
	private String u4321; // appliedProfileId
	private String w0792; // version
	private String w0793; // versionMajor
	private String w0794; // versionMinor
	private String w0795; // assignedApplicationEndpoint
	private String w0796; // securityKey
	private String w0797; // applicationData
	private String w0798; // applicationPort
	private String w0799; // state

	// aditional data
    private String k0872; // applicationName
    private String k0880; // applicationImage    
    
	public String getW0791() {
		return w0791;
	}
	public void setW0791(String w0791) {
		this.w0791 = w0791;
	}
	public String getB4321() {
		return b4321;
	}
	public void setB4321(String b4321) {
		this.b4321 = b4321;
	}
	public String getK0871() {
		return k0871;
	}
	public void setK0871(String k0871) {
		this.k0871 = k0871;
	}
	public String getU4321() {
		return u4321;
	}
	public void setU4321(String u4321) {
		this.u4321 = u4321;
	}
	public String getW0792() {
		return w0792;
	}
	public void setW0792(String w0792) {
		this.w0792 = w0792;
	}
	public String getW0793() {
		return w0793;
	}
	public void setW0793(String w0793) {
		this.w0793 = w0793;
	}
	public String getW0794() {
		return w0794;
	}
	public void setW0794(String w0794) {
		this.w0794 = w0794;
	}
	public String getW0795() {
		return w0795;
	}
	public void setW0795(String w0795) {
		this.w0795 = w0795;
	}
	public String getW0796() {
		return w0796;
	}
	public void setW0796(String w0796) {
		this.w0796 = w0796;
	}
	public String getW0797() {
		return w0797;
	}
	public void setW0797(String w0797) {
		this.w0797 = w0797;
	}
	public String getW0798() {
		return w0798;
	}
	public void setW0798(String w0798) {
		this.w0798 = w0798;
	}
	public String getW0799() {
		return w0799;
	}
	public void setW0799(String w0799) {
		this.w0799 = w0799;
	}
	public String getK0872() {
		return k0872;
	}
	public void setK0872(String k0872) {
		this.k0872 = k0872;
	}
	public String getK0880() {
		return k0880;
	}
	public void setK0880(String k0880) {
		this.k0880 = k0880;
	}
	
	@Override
	public String toString() {
		return "AppSubscription [w0791=" + w0791 + ", b4321=" + b4321 + ", k0871=" + k0871 + ", u4321=" + u4321
				+ ", w0792=" + w0792 + ", w0793=" + w0793 + ", w0794=" + w0794 + ", w0795=" + w0795 + ", w0796=" + w0796
				+ ", w0797=" + w0797 + ", w0798=" + w0798 + ", k0872=" + k0872 + ", k0880=" + k0880 + "]";
	}		
}
