package pe.olympus.repository;

import pe.olympus.model.SubsConfig;
import pe.olympus.model.Subscription;

public interface SubscriptionRepository {
	
	Subscription getSubscriptionById (String subscriptionId);
	Subscription updateSubscriptionDerivativeKey (String subscriptionId, String derivativeKey);
	SubsConfig getConfiguration();
	
}
