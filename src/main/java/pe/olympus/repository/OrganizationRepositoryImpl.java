package pe.olympus.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.gson.Gson;

import pe.olympus.constants.Constants;
import pe.olympus.model.CipherRequest;
import pe.olympus.model.Organization;
import pe.olympus.model.ResponseObject;
import pe.olympus.service.CryptoService;


@Repository
public class OrganizationRepositoryImpl implements OrganizationRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Firestore firebaseClient;
	private ObjectMapper mapper;
	private Gson gsonTemplate;
	private CryptoService cryptoService;
	
	@Autowired
	public OrganizationRepositoryImpl(Firestore firebaseClient,
			ObjectMapper mapper,
			Gson gsonTemplate,
			CryptoService cryptoService) {
		
		this.firebaseClient = firebaseClient;
		this.mapper = mapper;
		this.gsonTemplate = gsonTemplate;
		this.cryptoService = cryptoService;
	}

	@Override
	public Organization getOrganizationBySubscriptionId(String subscriptionId) {
		
		try {
			Organization organization = null;
			
			DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.document(subscriptionId)
					.get().get();
			
			if(subscriptionDocument.exists()) {
				
				DocumentSnapshot organizationDocument = firebaseClient.collection(Constants.T_ORGANIZATIONS)
						.document(subscriptionDocument.getString("f4461"))
						.get().get();
				organization = mapper.convertValue(organizationDocument.getData(), Organization.class);
				organization.setF4461(organizationDocument.getId());
				
				getOrganizationValues(organization, subscriptionDocument.getString("b4325"));
				
				logger.debug("OrganizationRepository.getOrganizationBySubscriptionId convertValue() #organization : {}", gsonTemplate.toJson(organization));
				
				logger.info("OrganizationRepository.getOrganizationBySubscriptionId : getOrganizationBySubscriptionId() is done");
				logger.debug("OrganizationRepository.getOrganizationBySubscriptionId #organization : {}", gsonTemplate.toJson(organization));
				
			}
			
			return organization;
			
		}catch (Exception e) {
			logger.error("OrganizationRepository.getOrganizationBySuscriptionId #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	public void getOrganizationValues(Organization organization, String indexKey) {
		
		organization.setF4462(defaultDecrypt(organization.getF4462(), indexKey));
		organization.setF4463(defaultDecrypt(organization.getF4463(), indexKey));
		organization.setF4464(defaultDecrypt(organization.getF4464(), indexKey));
		organization.setF4469(defaultDecrypt(organization.getF4469(), indexKey));
		organization.setF4470(defaultDecrypt(organization.getF4470(), indexKey));
		organization.setF4471(defaultDecrypt(organization.getF4471(), indexKey));
		organization.setF4472(defaultDecrypt(organization.getF4472(), indexKey));
		organization.setF4473(defaultDecrypt(organization.getF4473(), indexKey));
		organization.setF4474(defaultDecrypt(organization.getF4474(), indexKey));
		organization.setF4475(defaultDecrypt(organization.getF4475(), indexKey));
	}	
	
	public String defaultDecrypt (String text, String indexKey) {
	
		try {
			
			CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(text, indexKey);
			
			ResponseObject response = cryptoService.decrypt(cipherRequest);
			
			return response.getDataList().get(0);
			
		} catch (Exception e) {
			
			throw new RuntimeException(e.getMessage());
		}
	}
}
