package pe.olympus.repository;

import java.util.List;

import pe.olympus.model.AppComponent;
import pe.olympus.model.AppInstance;
import pe.olympus.model.Application;
import pe.olympus.model.ApplicationDTO;
import pe.olympus.model.GenericResponse;

public interface ApplicationRepository {
	
	List<Application> getApplications(String userId);
	GenericResponse createApplication(Application application);
	GenericResponse createInstance(AppInstance appInstance);
	GenericResponse updateInstance(AppInstance appInstance);
	List<AppComponent> getAppComponentsByAppSubscriptionId(String appSubscriptionId, String appShortName);
	List<ApplicationDTO> getAllApplications();
}
