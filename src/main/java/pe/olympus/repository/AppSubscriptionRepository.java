package pe.olympus.repository;

import java.util.List;

import pe.olympus.model.BindDTO;
import pe.olympus.model.AgentDTO;
import pe.olympus.model.AppInstance;
import pe.olympus.model.AppSubsComponent;
import pe.olympus.model.AppSubscription;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.VerifyResponse;

public interface AppSubscriptionRepository {

	List<AppSubscription> getAppsSubscriptionBySubscriptionId(String subscriptionId);	
	AppSubscription getAppSubscription(String subscriptionId, String applicationId);
	AppSubscription getAppSubscriptionById(String appSubscriptionId);
	AppSubscription updateAppSubscriptionDerivativeKey(String appSubscriptionId, String derivativeKey);
	VerifyResponse verifySubscriptionByIndexKey(String indexKey, String workingApplicationKey);
	GenericResponse createAppSubscription(AppSubscription appSubscription);
	VerifyResponse verifySubscription(BindDTO bindDTO);
	GenericResponse saveApplicationData(AgentDTO agentDTO);
	GenericResponse createAppSubsComponents(List<AppSubsComponent> listAppSubsComponents);
	AppInstance getAppInstanceByAppSubscriptionId(String appSubscriptionId);
}
