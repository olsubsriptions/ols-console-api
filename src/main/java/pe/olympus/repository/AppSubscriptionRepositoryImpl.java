package pe.olympus.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import pe.olympus.constants.ApplicationCode;
import pe.olympus.constants.Constants;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.BindDTO;
import pe.olympus.model.BindDataDTO;
import pe.olympus.model.AgentCipherDTO;
import pe.olympus.model.AgentDTO;
import pe.olympus.model.AppInstance;
import pe.olympus.model.AppSubsComponent;
import pe.olympus.model.AppSubsInstance;
import pe.olympus.model.AppSubscription;
import pe.olympus.model.CipherRequest;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.ResponseObject;
import pe.olympus.model.VerifyResponse;
import pe.olympus.service.CryptoService;

@Repository
public class AppSubscriptionRepositoryImpl implements AppSubscriptionRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private Firestore firebaseClient;
	private ObjectMapper mapper;
	private CryptoService cryptoService;
	private MysqlRepository mysqlRepository;
	private Gson gsonTemplate; 
	
	@Autowired
	public AppSubscriptionRepositoryImpl(Firestore firebaseClient,
			ObjectMapper mapper,
			CryptoService cryptoService,
			MysqlRepository mysqlRepository,
			Gson gsonTemplate) {
		this.firebaseClient = firebaseClient;
		this.mapper = mapper;
		this.cryptoService = cryptoService;
		this.mysqlRepository = mysqlRepository;
		this.gsonTemplate = gsonTemplate;
	}

	@Override
	public List<AppSubscription> getAppsSubscriptionBySubscriptionId(String subscriptionId){
		
		try {
			
			List<AppSubscription> listAppSubscription = null;
			QuerySnapshot querySnapshot = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
					.whereEqualTo("b4321", subscriptionId).get().get();
			
			List<QueryDocumentSnapshot> appSubscriptionDocuments = querySnapshot.getDocuments();
			
			if(!appSubscriptionDocuments.isEmpty()) {			
				listAppSubscription = new ArrayList<AppSubscription>();
				
				for(QueryDocumentSnapshot appSubscriptionDocument : appSubscriptionDocuments) {
					DocumentSnapshot applicationDocument =firebaseClient.collection(Constants.T_APPLICATIONS)
							.document(appSubscriptionDocument.getString("k0871"))
							.get().get();
					
					AppSubscription appSubscription = mapper.convertValue(appSubscriptionDocument.getData(), AppSubscription.class);
					appSubscription.setW0791(appSubscriptionDocument.getId());
					appSubscription.setK0872(applicationDocument.getString("k0872"));
					appSubscription.setK0880(applicationDocument.getString("k0880"));					
					logger.debug("AppSubscriptionRepository.getAppsSubscriptionBySubscriptionId convertValue() #appSubscription : {}", gsonTemplate.toJson(appSubscription));
					
					listAppSubscription.add(appSubscription);
				}
			}
			
			logger.info("AppSubscriptionRepository.getAppsSubscriptionBySubscriptionId : getAppsSubscriptionBySubscriptionId() is done");
			logger.debug("AppSubscriptionRepository.getAppsSubscriptionBySubscriptionId #listAppSubscription : {}", gsonTemplate.toJson(listAppSubscription));
			return listAppSubscription;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.getAppsSubscriptionBySubscriptionId #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public AppSubscription getAppSubscription(String subscriptionId, String applicationId){
		
		try {
			
			AppSubscription appSubscription = null;
			QuerySnapshot querySnapshot = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
				.whereEqualTo("b4321", subscriptionId)
				.whereEqualTo("k0871", applicationId)
				.get().get();
			
			List<QueryDocumentSnapshot> appSubscriptionDocuments = querySnapshot.getDocuments();
			
			if(!appSubscriptionDocuments.isEmpty()) {
				appSubscription = mapper.convertValue(appSubscriptionDocuments.get(0).getData(), AppSubscription.class);
				appSubscription.setW0791(appSubscriptionDocuments.get(0).getId());
				logger.debug("AppSubscriptionRepository.getAppSubscription convertValue() #appSubscription : {}", gsonTemplate.toJson(appSubscription));
			}
			
			logger.info("AppSubscriptionRepository.getAppSubscription : getAppSubscription() is done");
			logger.debug("AppSubscriptionRepository.getAppSubscription #appSubscription : {}", gsonTemplate.toJson(appSubscription));
			return appSubscription;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.getAppSubscription #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}
	
	@Override
	public AppSubscription getAppSubscriptionById(String appSubscriptionId) {
		
		try {			
			
			DocumentSnapshot appSubscriptionDocument  = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
					.document(appSubscriptionId)
					.get().get();
			
			AppSubscription appSubscription = mapper.convertValue(appSubscriptionDocument.getData(), AppSubscription.class);
			appSubscription.setW0791(appSubscriptionDocument.getId());			
			logger.debug("AppSubscriptionRepository.getAppSubscriptionById convertValue() #appSubscription : {}", gsonTemplate.toJson(appSubscription));
			
			logger.info("AppSubscriptionRepository.getAppSubscriptionById : getAppSubscriptionById() is done");
			logger.debug("AppSubscriptionRepository.getAppSubscriptionById #appSubscription : {}", gsonTemplate.toJson(appSubscription));
			return appSubscription;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.getAppSubscriptionById #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public AppSubscription updateAppSubscriptionDerivativeKey(String appSubscriptionId, String derivativeKey) {
		
		try {
			AppSubscription appSubscription = null;
			DocumentReference appSubscriptionReference = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
					.document(appSubscriptionId);	
			
			WriteResult writeResult = appSubscriptionReference.update("w0796", derivativeKey).get();

			if(writeResult != null) {
				DocumentSnapshot appSubscriptionDocument = appSubscriptionReference.get().get();				
				appSubscription = mapper.convertValue(appSubscriptionDocument.getData(), AppSubscription.class);
				appSubscription.setW0791(appSubscriptionDocument.getId());
				logger.debug("AppSubscriptionRepository.updateAppSubscriptionDerivativeKey convertValue() #appSubscription : {}", gsonTemplate.toJson(appSubscription));
			}
			
			logger.info("AppSubscriptionRepository.updateAppSubscriptionDerivativeKey : updateAppSubscriptionDerivativeKey() is done");
			logger.debug("AppSubscriptionRepository.updateAppSubscriptionDerivativeKey #appSubscription : {}", gsonTemplate.toJson(appSubscription));
			return appSubscription;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.updateAppSubscriptionDerivativeKey #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public VerifyResponse verifySubscriptionByIndexKey(String indexKey, String workingApplicationKey){
		
		try {
			
			VerifyResponse verifyResponse = new VerifyResponse();
			verifyResponse.setStatus(false);
			verifyResponse.setMessage(ResponseMessages.SUSCRIPCION_INVALIDA);
			
			QuerySnapshot querySnapshotS = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.whereEqualTo("b4325", indexKey)
					.get().get();
			
			List<QueryDocumentSnapshot> subscriptionDocuments = querySnapshotS.getDocuments();
			
			if(!subscriptionDocuments.isEmpty()) {
				QuerySnapshot querySnapshotAS = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
						.whereEqualTo("b4321", subscriptionDocuments.get(0).getId())
						.whereEqualTo("w0796", workingApplicationKey)
						.get().get();
				
				if(!querySnapshotAS.isEmpty()) {
					
					List<QueryDocumentSnapshot> listAppSubscriptionDocument = querySnapshotAS.getDocuments();
					
					QuerySnapshot querySnapshotSI = firebaseClient.collection(Constants.T_SUBSCRIPTION_INSTANCES)
							.whereEqualTo("w0791", listAppSubscriptionDocument.get(0).getId())
							.get().get();
					
					List<QueryDocumentSnapshot> listAppSubsInstanceDocument = querySnapshotSI.getDocuments();
					
					AppSubsInstance appSubsInstance = mapper.convertValue(listAppSubsInstanceDocument.get(0).getData(), AppSubsInstance.class);
					appSubsInstance.setD0731(listAppSubsInstanceDocument.get(0).getId());;
					logger.debug("AppSubscriptionRepository.verifySubscriptionByIndexKey convertValue() #appSubsInstance : {}", gsonTemplate.toJson(appSubsInstance));
					
					DocumentSnapshot appInstanceDocument = firebaseClient.collection(Constants.T_INSTANCES)
							.document(appSubsInstance.getI7071())
							.get().get();
					
					AppInstance appInstance = mapper.convertValue(appInstanceDocument.getData(), AppInstance.class);
					appInstance.setI7071(appInstanceDocument.getId());
					appInstance.setD0732(appSubsInstance.getD0732());
					logger.debug("AppSubscriptionRepository.verifySubscriptionByIndexKey convertValue() #appInstance : {}", gsonTemplate.toJson(appInstance));
					
					verifyResponse.setStatus(true);
					verifyResponse.setMessage(ResponseMessages.SUSCRIPCION_VALIDA);
					verifyResponse.setAppInstance(appInstance);
				}
			}
			
			logger.info("AppSubscriptionRepository.verifySubscriptionByIndexKey : verifySubscriptionByIndexKey() is done");
			logger.debug("AppSubscriptionRepository.verifySubscriptionByIndexKey #verifyResponse : {}", gsonTemplate.toJson(verifyResponse));
			return verifyResponse;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.verifySubscriptionByIndexKey #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		
	}

	@Override
	public GenericResponse createAppSubscription(AppSubscription appSubscription) {
		
		try {
			
			appSubscription.setW0791(UUID.randomUUID().toString());
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
			
			QuerySnapshot querySnapshotAS = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
					.whereEqualTo("b4321", appSubscription.getB4321())
					.whereEqualTo("k0871", appSubscription.getK0871())
					.get().get();
			
			if (querySnapshotAS.isEmpty()) {
				DocumentSnapshot applicationDocument = firebaseClient.collection(Constants.T_APPLICATIONS)
						.document(appSubscription.getK0871()).get().get();
				
				if(applicationDocument.exists()) {
					
					appSubscription.setU4321(applicationDocument.getString("u4321"));
					appSubscription.setW0792(applicationDocument.getString("k0874"));
					appSubscription.setW0793(applicationDocument.getString("k0875"));
					appSubscription.setW0794(applicationDocument.getString("k0876"));
					appSubscription.setW0795(applicationDocument.getString("k0877"));
					appSubscription.setW0796(cryptoService.getKey());
					appSubscription.setW0797("appData");
					appSubscription.setW0798(applicationDocument.getString("k0878"));
					appSubscription.setW0799(applicationDocument.getString("k0873"));
					
					Map<String, Object> appSubscriptionData = new HashMap<>();
					appSubscriptionData.put("b4321", appSubscription.getB4321());
					appSubscriptionData.put("k0871", appSubscription.getK0871());
					appSubscriptionData.put("u4321", appSubscription.getU4321());			
					appSubscriptionData.put("w0792", appSubscription.getW0792());
					appSubscriptionData.put("w0793", appSubscription.getW0793());
					appSubscriptionData.put("w0794", appSubscription.getW0794());
					appSubscriptionData.put("w0795", appSubscription.getW0795());
					appSubscriptionData.put("w0796", appSubscription.getW0796());
					appSubscriptionData.put("w0797", appSubscription.getW0797());
					appSubscriptionData.put("w0798", appSubscription.getW0798());
					appSubscriptionData.put("w0799", appSubscription.getW0799());
					
					DocumentReference appSubscriptionRef = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
					.document(appSubscription.getW0791());
					
					WriteResult appSubscriptionWR = appSubscriptionRef.create(appSubscriptionData).get();
					
					if(appSubscriptionWR != null) {
						
						QuerySnapshot querySnapshotI = firebaseClient.collection(Constants.T_INSTANCES)
								.whereEqualTo("k0871", appSubscription.getK0871())
								.whereEqualTo("i7074", true)
								.get().get();
						
						if(!querySnapshotI.isEmpty()) {
							QueryDocumentSnapshot selectedInstanceDocument = null;
							List<QueryDocumentSnapshot> instanceDocuments = querySnapshotI.getDocuments();
							for(QueryDocumentSnapshot instanceDocument : instanceDocuments) {
								if(selectedInstanceDocument == null) {
									selectedInstanceDocument = instanceDocument;
								}
								else {
									selectedInstanceDocument = selectedInstanceDocument.getDouble("i7075") <= instanceDocument.getDouble("i7075") ? selectedInstanceDocument : instanceDocument;
								}
							}
							
							AppInstance appInstance = new AppInstance();
							
							appInstance = mapper.convertValue(selectedInstanceDocument.getData(), AppInstance.class);
							appInstance.setI7071(selectedInstanceDocument.getId());
							logger.debug("AppSubscriptionRepository.createAppSubscription convertValue() #appInstance : {}", gsonTemplate.toJson(appInstance));
							
							String queueName = "COLA";
							
							Map<String, String> subsInstanceData = new HashMap<>();
							subsInstanceData.put("i7071", appInstance.getI7071());
							subsInstanceData.put("w0791", appSubscription.getW0791());
							subsInstanceData.put("d0732", queueName);
							
							DocumentReference subsInstanceRef = firebaseClient.collection(Constants.T_SUBSCRIPTION_INSTANCES)
									.document(UUID.randomUUID().toString());
							
							WriteResult subsInstanceWR = subsInstanceRef.create(subsInstanceData).get();
							
							if(subsInstanceWR != null) {
								
								DocumentReference instanceRef = firebaseClient.collection(Constants.T_INSTANCES)
										.document(appInstance.getI7071());
								
								WriteResult instanceWR = instanceRef.update("i7075", appInstance.getI7075() + 1).get();
								
								if(instanceWR != null) {
								
									switch (applicationDocument.getString("k0873")) {
										case ApplicationCode.MONETA: {
											
											DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
													.document(appSubscription.getB4321())
													.get().get();
										
											DocumentSnapshot organizationDocument = firebaseClient.collection(Constants.T_ORGANIZATIONS)
													.document(subscriptionDocument.getString("f4461"))
													.get().get();
											
											String indexKey = subscriptionDocument.getString("b4325");
											String organizationName = organizationDocument.getString("f4462");
											
											CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(organizationName, indexKey);
											
											ResponseObject response = cryptoService.decrypt(cipherRequest);
											
											organizationName = response.getDataList().get(0);
										
											genericResponse = mysqlRepository.saveOrganization(appInstance, indexKey, organizationName, queueName);
											break;
										}
										case ApplicationCode.TRANSLINK_TRANSACTION_SERVICES: {
											genericResponse.setStatus(true);
											genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
											break;
										}
										case ApplicationCode.SECURITY_PROTECCION_SUITE: {
											genericResponse.setStatus(true);
											genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
											break;
										}
									}														
								}
							}					
						}						
					}		
					
					logger.info("AppSubscriptionRepository.createAppSubscription : createAppSubscription() is done");
					logger.debug("AppSubscriptionRepository.createAppSubscription #genericResponse : {}", gsonTemplate.toJson(genericResponse));
					genericResponse.setStatus(true);
					genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
					genericResponse.setData(appSubscription.getW0791());
					return genericResponse;
				}				
				genericResponse.setMessage(ResponseMessages.APLICACION_NOEXISTE);
				
				logger.info("AppSubscriptionRepository.createAppSubscription : createAppSubscription() is done");
				logger.debug("AppSubscriptionRepository.createAppSubscription #genericResponse : {}", gsonTemplate.toJson(genericResponse));
				return genericResponse;
			}	
			genericResponse.setMessage(ResponseMessages.SUSCRIPCION_EXISTE);
			
			logger.info("AppSubscriptionRepository.createAppSubscription : createAppSubscription() is done");
			logger.debug("AppSubscriptionRepository.createAppSubscription #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.createAppSubscription #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public VerifyResponse verifySubscription(BindDTO bindDTO){
		
		try {
			
			VerifyResponse verifyResponse = new VerifyResponse();
			verifyResponse.setStatus(false);
			verifyResponse.setMessage(ResponseMessages.SUSCRIPCION_INVALIDA);
			
			QuerySnapshot querySnapshotS = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.whereEqualTo("b4325", bindDTO.getIndex_key()).get().get();
			
			if(!querySnapshotS.isEmpty()) {	
				
				CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(bindDTO.getData(), null);
				
				ResponseObject response = cryptoService.decrypt(cipherRequest);
				
				if(response.getDataList().size() > 0) {
					
					BindDataDTO bindData = gsonTemplate.fromJson(response.getDataList().get(0), BindDataDTO.class);
					
					if(bindData.getApplication_id() != null && bindData.getSubscription_id() != null) {
						
						DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
								.document(bindData.getSubscription_id())
								.get().get();
						DocumentSnapshot appSubscriptionDocument = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
								.document(bindData.getApplication_id())
								.get().get();
						if(subscriptionDocument.get("b4324").equals(bindData.getWorking_subscription_derived_key())
								&&
								appSubscriptionDocument.get("w0796").equals(bindData.getWorking_application_derived_key())) {
							
							verifyResponse.setStatus(true);
							verifyResponse.setMessage(ResponseMessages.SUSCRIPCION_VALIDA);
						}
					}					
				}
			}
			
			logger.info("AppSubscriptionRepository.verifySubscription : verifySubscription() is done");
			logger.debug("AppSubscriptionRepository.verifySubscription #verifyResponse : {}", gsonTemplate.toJson(verifyResponse));
			return verifyResponse;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.verifySubscription #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public GenericResponse createAppSubsComponents(List<AppSubsComponent> listAppSubsComponents) {
		
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
			
			for (AppSubsComponent appSubsComponent : listAppSubsComponents) {
				
				QuerySnapshot querySnapshotSC = firebaseClient.collection(Constants.T_SUBSCRIPTION_COMPONENTS)
						.whereEqualTo("w0791", appSubsComponent.getW0791())
						.whereEqualTo("y9072", appSubsComponent.getY9072())
						.get().get();
				
				if(querySnapshotSC.isEmpty()) {
					appSubsComponent.setY9071(UUID.randomUUID().toString());
					
					Map<String, Object> appSubsComponentData = new HashMap<>();
					appSubsComponentData.put("w0791", appSubsComponent.getW0791());
					appSubsComponentData.put("c7051", appSubsComponent.getC7051());
					appSubsComponentData.put("y9072", appSubsComponent.getY9072());
					appSubsComponentData.put("y9073", appSubsComponent.getY9073());
					appSubsComponentData.put("y9074", appSubsComponent.getY9074());
					appSubsComponentData.put("y9075", appSubsComponent.getY9075());			
					appSubsComponentData.put("y9076", appSubsComponent.getY9076());
					
					DocumentReference appSubsComponentRef = firebaseClient.collection(Constants.T_SUBSCRIPTION_COMPONENTS)
							.document(appSubsComponent.getY9071());
					WriteResult writeResult = appSubsComponentRef.create(appSubsComponentData).get();
					
					if(writeResult != null) {
						genericResponse.setStatus(true);
						genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
					}
					
				}
				else {
					
					DocumentReference appSubsComponentRef = firebaseClient.collection(Constants.T_SUBSCRIPTION_COMPONENTS)
							.document(querySnapshotSC.getDocuments().get(0).getId());
					WriteResult writeResult = appSubsComponentRef.update("y9075", appSubsComponent.getY9075()).get();
					
					if(writeResult != null) {
						genericResponse.setStatus(true);
						genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
					}
				}
			}		
			
			logger.info("AppSubscriptionRepository.createAppSubsComponents : createAppSubsComponents() is done");
			logger.debug("AppSubscriptionRepository.createAppSubsComponents #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;	
		
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.createAppSubsComponents #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}	
	}

	@Override
	public AppInstance getAppInstanceByAppSubscriptionId(String appSubscriptionId) {
		
		try {			
			AppInstance appInstance = null;
			QuerySnapshot querySnapshotSI = firebaseClient.collection(Constants.T_SUBSCRIPTION_INSTANCES)
					.whereEqualTo("w0791", appSubscriptionId)
					.get().get();
			List<QueryDocumentSnapshot> listAppSubsInstance = querySnapshotSI.getDocuments();
			
			if(!listAppSubsInstance.isEmpty()) {				
				DocumentSnapshot appInstanceDocument = firebaseClient.collection(Constants.T_INSTANCES)
						.document(listAppSubsInstance.get(0).getString("i7071"))
						.get().get();
				appInstance = mapper.convertValue(appInstanceDocument.getData(), AppInstance.class);
				appInstance.setI7071(appInstanceDocument.getId());				
				logger.debug("AppSubscriptionRepository.getAppInstanceByAppSubscriptionId convertValue() #appInstance : {}", gsonTemplate.toJson(appInstance));
			}
			
			logger.info("AppSubscriptionRepository.getAppInstanceByAppSubscriptionId : getAppInstanceByAppSubscriptionId() is done");
			logger.debug("AppSubscriptionRepository.getAppInstanceByAppSubscriptionId #appInstance : {}", gsonTemplate.toJson(appInstance));
			return appInstance;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.getAppInstanceByAppSubscriptionId #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse saveApplicationData(AgentDTO agentDTO) {
		
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.CIPHER_STRUCTURE_ERROR);
			
			if(validateCipherStructure(agentDTO.getCipher())) {
				
				genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);				
				QuerySnapshot querySnapshotS = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
						.whereEqualTo("b4325", agentDTO.getIndex_key()).get().get();
				
				if(!querySnapshotS.isEmpty()) {
					
					String subscriptionId = querySnapshotS.getDocuments().get(0).getId();
					
					QuerySnapshot querySnapshotAS = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
							.whereEqualTo("b4321", subscriptionId)
							.whereEqualTo("w0796", agentDTO.getWorking_application_key())
							.get().get();
					if(!querySnapshotAS.isEmpty()) {					
						
						DocumentReference appSubscriptionReference = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
								.document(querySnapshotAS.getDocuments().get(0).getId());
						
						WriteResult writeResult = appSubscriptionReference.update("w0797", agentDTO.getCipher()).get();
						
						if(writeResult != null) {
							
							DocumentSnapshot applicationDocument = firebaseClient.collection(Constants.T_APPLICATIONS)
									.document(appSubscriptionReference.get().get().getString("k0871"))
									.get().get();
							
							switch (applicationDocument.getString("k0873")) {
								case ApplicationCode.MONETA : {
									genericResponse.setStatus(true);
									genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
									break;
								}
								case ApplicationCode.TRANSLINK_TRANSACTION_SERVICES : {
									genericResponse.setStatus(true);
									genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
									break;
								}
								case ApplicationCode.SECURITY_PROTECCION_SUITE : {
									genericResponse.setStatus(true);
									genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
									break;
								}
							}							
						}
					}
					
				}
			}
			
			logger.info("AppSubscriptionRepository.saveApplicationData : saveApplicationData() is done");
			logger.debug("AppSubscriptionRepository.saveApplicationData #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("AppSubscriptionRepository.saveApplicationData #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}
	
	public boolean validateCipherStructure(String cipher) {
		
		try {
			
			boolean valid = true;
			
			CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(cipher, null);
			
			ResponseObject response = cryptoService.decrypt(cipherRequest);
			
			List<AgentCipherDTO> listAgentCipherDTO = gsonTemplate.fromJson(response.getDataList().get(0), new TypeToken<ArrayList<AgentCipherDTO>>(){}.getType());
			
			for(AgentCipherDTO agentCipherDTO : listAgentCipherDTO) {
				if(agentCipherDTO.isNull()) {
					valid = false;
					break;
				}
			}
			
			logger.info("AppSubscriptionRepository.validateCipherStructure : validateCipherStructure() is done");
			logger.debug("AppSubscriptionRepository.validateCipherStructure #valid : {}", gsonTemplate.toJson(valid));
			return valid;
			
		} catch (Exception e){
			logger.error("AppSubscriptionRepository.validateCipherStructure #error : {}", e.getMessage());
			return false;
		}
		
	}
}
