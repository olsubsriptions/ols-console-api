package pe.olympus.repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.gson.Gson;

import pe.olympus.constants.Constants;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.AppComponent;
import pe.olympus.model.AppInstance;
import pe.olympus.model.AppSubsComponent;
import pe.olympus.model.Application;
import pe.olympus.model.ApplicationDTO;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.UserS;

@Repository
public class ApplicationRepositoryImpl implements ApplicationRepository {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private Firestore firebaseClient;
	private ObjectMapper mapper;
	private UserRepository userRepository;
	private Gson gsonTemplate;
	
	@Autowired
	public ApplicationRepositoryImpl(
			Firestore firebaseClient,
			ObjectMapper mapper,
			UserRepository userRepository,
			Gson gsonTemplate) {
		
		this.firebaseClient = firebaseClient;
		this.mapper = mapper;
		this.userRepository = userRepository;
		this.gsonTemplate = gsonTemplate;
	}

	@Override
	public List<Application> getApplications(String userId){
		
		try {
			
			List<Application> listApplication = null;
			UserS user = userRepository.getUserById(userId);
			
			if(user != null) {
				
				QuerySnapshot querySnapshot = firebaseClient.collection(Constants.T_APPLICATIONS)
						.whereEqualTo("k0879", true)
						.get().get();
				List<QueryDocumentSnapshot> applicationDocuments = querySnapshot.getDocuments();
				
				if(!applicationDocuments.isEmpty()) {			
					listApplication = new ArrayList<Application>();
					
					for(QueryDocumentSnapshot applicationDocument : applicationDocuments) {
						
						QuerySnapshot querySnapshotAS = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
								.whereEqualTo("b4321", user.getB4321())
								.whereEqualTo("k0871", applicationDocument.getId())
								.get().get();
						
						if(querySnapshotAS.isEmpty()) {
							
							Application application = mapper.convertValue(applicationDocument.getData(), Application.class);
							application.setK0871(applicationDocument.getId());
							
							logger.debug("ApplicationRepository.getApplications convertValue() #application : {}", gsonTemplate.toJson(application));
							listApplication.add(application);
						}						
					}
				}
			}
			logger.info("ApplicationRepository.getApplications : getApplications() is done");
			logger.debug("ApplicationRepository.getApplications #listApplication : {}", gsonTemplate.toJson(listApplication));
			return listApplication;
			
		} catch (Exception e) {
			logger.error("ApplicationRepository.getApplications #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}
	
	@Override
	public List<ApplicationDTO> getAllApplications() {
		try {
			
			List<ApplicationDTO> listApplication = null;
			
			QuerySnapshot querySnapshotA = firebaseClient.collection(Constants.T_APPLICATIONS).get().get();
			List<QueryDocumentSnapshot> applicationDocuments = querySnapshotA.getDocuments();
			
			if(!applicationDocuments.isEmpty()) {
				listApplication = new ArrayList<ApplicationDTO>();
				for(QueryDocumentSnapshot applicationDocument : applicationDocuments) {
					ApplicationDTO application = mapper.convertValue(applicationDocument.getData(), ApplicationDTO.class);
					application.setK0871(applicationDocument.getId());					
					logger.debug("ApplicationRepository.getAllApplications convertValue() #application : {}", gsonTemplate.toJson(application));
					
					QuerySnapshot querySnapshotAI = firebaseClient.collection(Constants.T_INSTANCES)
							.whereEqualTo("k0871", application.getK0871())
							.get().get();
					List<QueryDocumentSnapshot> appInstanceDocuments = querySnapshotAI.getDocuments();
					if(!appInstanceDocuments.isEmpty()) {
						List<AppInstance> listAppInstance = new ArrayList<AppInstance>();
						for(QueryDocumentSnapshot appInstanceDocument : appInstanceDocuments) {
							AppInstance appInstance = mapper.convertValue(appInstanceDocument.getData(), AppInstance.class);
							appInstance.setI7071(appInstanceDocument.getId());
							logger.debug("ApplicationRepository.getAllApplications convertValue() #appInstance : {}", gsonTemplate.toJson(appInstance));
							
							listAppInstance.add(appInstance);
						}
						application.setInstances(listAppInstance);
					}
					
					QuerySnapshot querySnapshotAC = firebaseClient.collection(Constants.T_COMPONENTS)
							.whereEqualTo("k0871", application.getK0871())
							.get().get();
					List<QueryDocumentSnapshot> appComponentDocuments = querySnapshotAC.getDocuments();
					if(!appComponentDocuments.isEmpty()) {
						List<AppComponent> listAppComponent = new ArrayList<AppComponent>();
						for(QueryDocumentSnapshot appComponentDocument : appComponentDocuments) {
							AppComponent appComponent = mapper.convertValue(appComponentDocument.getData(), AppComponent.class);
							appComponent.setC7051(appComponentDocument.getId());
							logger.debug("ApplicationRepository.getAllApplications convertValue() #appComponent : {}", gsonTemplate.toJson(appComponent));
							
							listAppComponent.add(appComponent);
						}
						application.setComponents(listAppComponent);
					}
					
					listApplication.add(application);
				}
			}
			
			logger.info("ApplicationRepository.getAllApplications : getAllApplications() is done");
			logger.debug("ApplicationRepository.getAllApplications #listApplication : {}", gsonTemplate.toJson(listApplication));
			return listApplication;
		} catch (Exception e) {
			
			logger.error("ApplicationRepository.getAllApplications #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		
		
	}	

	@Override
	public GenericResponse createApplication(Application application){
		
		try {
			
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			
			application.setK0871(UUID.randomUUID().toString());
			
			Map<String, Object> applicationData = new HashMap<>();
			applicationData.put("k0872", application.getK0872());
			applicationData.put("u4321", application.getU4321());
			applicationData.put("k0873", application.getK0873());
			applicationData.put("k0874", application.getK0874());
			applicationData.put("k0875", application.getK0875());
			applicationData.put("k0876", application.getK0876());
			applicationData.put("k0877", application.getK0877());
			applicationData.put("k0878", application.getK0878());
			applicationData.put("k0879", application.getK0879());
			applicationData.put("k0880", application.getK0880());		
			
			DocumentReference applicationDocument = firebaseClient.collection(Constants.T_APPLICATIONS)
					.document(application.getK0871());
			WriteResult writeResult = applicationDocument.create(applicationData).get();
			
			if(writeResult != null) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}			
			
			logger.info("ApplicationRepository.createApplication : createApplication() is done");
			logger.debug("ApplicationRepository.createApplication #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("ApplicationRepository.createApplication #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Override
	public GenericResponse createInstance(AppInstance appInstance) {
		
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
			
			appInstance.setI7071(UUID.randomUUID().toString());
			
			Map<String, Object> appInstanceData = new HashMap<>();
			appInstanceData.put("i7072", appInstance.getI7072());
			appInstanceData.put("i7073", appInstance.getI7073());
			appInstanceData.put("i7074", appInstance.getI7074());
			appInstanceData.put("i7075", appInstance.getI7075());
			appInstanceData.put("i7076", appInstance.getI7076());
			appInstanceData.put("i7077", appInstance.getI7077());
			appInstanceData.put("k0871", appInstance.getK0871());
			
			DocumentReference appInstanceDocument = firebaseClient.collection(Constants.T_INSTANCES)
					.document(appInstance.getI7071());
			
			WriteResult writeResult = appInstanceDocument.create(appInstanceData).get();
			
			if(writeResult != null) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("ApplicationRepository.createInstance : createInstance() is done");
			logger.debug("ApplicationRepository.createInstance #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("ApplicationRepository.createInstance #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
	
	@Override
	public GenericResponse updateInstance(AppInstance appInstance) {
		
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
			
			Map<String, Object> appInstanceData = new HashMap<>();
			appInstanceData.put("i7072", appInstance.getI7072());
			appInstanceData.put("i7073", appInstance.getI7073());
			appInstanceData.put("i7074", appInstance.getI7074());
			appInstanceData.put("i7076", appInstance.getI7076());
			appInstanceData.put("i7077", appInstance.getI7077());
			
			DocumentReference appInstanceDocument = firebaseClient.collection(Constants.T_INSTANCES)
					.document(appInstance.getI7071());
			
			WriteResult writeResult = appInstanceDocument.update(appInstanceData).get();
			
			if(writeResult != null) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			
			logger.info("ApplicationRepository.updateInstance : updateInstance() is done");
			logger.debug("ApplicationRepository.updateInstance #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
		} catch (Exception e) {
			logger.error("ApplicationRepository.updateInstance #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<AppComponent> getAppComponentsByAppSubscriptionId(String appSubscriptionId, String appShortName) {
		
		try {			
			
			List<AppComponent> listAppComponent = null;
			DocumentSnapshot appSubscriptionDocument = firebaseClient.collection(Constants.T_APPLICATION_BY_SUBSCRIPTION)
					.document(appSubscriptionId)
					.get().get();			
			
			if(appSubscriptionDocument.get("w0799").equals(appShortName)) {
				QuerySnapshot querySnapshotC = firebaseClient.collection(Constants.T_COMPONENTS)
						.whereEqualTo("c7053", true)
						.whereEqualTo("k0871", appSubscriptionDocument.getString("k0871"))
						.get().get();
				
				List<QueryDocumentSnapshot> appComponentDocuments = querySnapshotC.getDocuments();
				
				if(!appComponentDocuments.isEmpty()) {
					
					listAppComponent = new ArrayList<AppComponent>();
					for(QueryDocumentSnapshot appComponentDocument : appComponentDocuments) {					
						
						List<AppSubsComponent> listAppSubsComponent = null;					
						QuerySnapshot querySnapshotASC = firebaseClient.collection(Constants.T_SUBSCRIPTION_COMPONENTS)
								.whereEqualTo("w0791", appSubscriptionDocument.getId())
								.whereEqualTo("c7051", appComponentDocument.getId())
								.get().get();
						System.out.println(appSubscriptionDocument.getId());
						System.out.println(appComponentDocument.getId());
						
						List<QueryDocumentSnapshot> appSubsComponentDocuments = querySnapshotASC.getDocuments();
						if(!appSubsComponentDocuments.isEmpty()) {
							listAppSubsComponent = new ArrayList<AppSubsComponent>();
							for(QueryDocumentSnapshot appSubsComponentDocument: appSubsComponentDocuments) {
								AppSubsComponent appSubsComponent = mapper.convertValue(appSubsComponentDocument.getData(), AppSubsComponent.class);
								appSubsComponent.setY9071(appSubsComponentDocument.getId());
								logger.debug("ApplicationRepository.getAppComponentsByApplicationId convertValue() #appSubsComponent : {}", gsonTemplate.toJson(appSubsComponent));
														
								listAppSubsComponent.add(appSubsComponent);
							}
						}
						
						AppComponent appComponent = mapper.convertValue(appComponentDocument.getData(), AppComponent.class);
						appComponent.setC7051(appComponentDocument.getId());
						logger.debug("ApplicationRepository.getAppComponentsByApplicationId convertValue() #appComponent : {}", gsonTemplate.toJson(appComponent));
						
						appComponent.setSubsComponents(listAppSubsComponent);					
						
						listAppComponent.add(appComponent);
					}				
				}
			}
			
			logger.info("ApplicationRepository.getAppComponentsByApplicationId : getAppComponentsByApplicationId() is done");
			logger.debug("ApplicationRepository.getAppComponentsByApplicationId #listAppComponent : {}", gsonTemplate.toJson(listAppComponent));
			return listAppComponent;
			
		} catch (Exception e) {
			logger.error("ApplicationRepository.getAppComponentsByApplicationId #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}	
}
