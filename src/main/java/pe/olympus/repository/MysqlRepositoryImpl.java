package pe.olympus.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.mysql.cj.jdbc.MysqlDataSource;

import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.AppInstance;
import pe.olympus.model.GenericResponse;

@Repository
public class MysqlRepositoryImpl implements MysqlRepository{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private MysqlDataSource mysqlDataSource;
	@Autowired
	private Gson gsonTemplate; 

	@Override
	public GenericResponse saveOrganization(AppInstance appInstance, String indexKey, String organizationName, String queueName) {
		
		GenericResponse genericResponse = new GenericResponse();
		genericResponse.setStatus(false);
		genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
		
		try {	
			
			String sql = "{call sp_o100_isus(? ,? ,?)}";
			
			Connection connection = getMysqlConnection(appInstance);
			
			CallableStatement statement = connection.prepareCall(sql);
			statement.setString("indexKey", indexKey);
			statement.setString("organizationName", organizationName);
			statement.setString("queueName", queueName);
			
			ResultSet result = statement.executeQuery();
			result.first();
			result.getString("id");
			
			String response = result.getString("id");
			
			result.close();
			statement.close();
			connection.close();
			
			if(response != null) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}	
			
			logger.info("MysqlRepository.saveOrganization : saveOrganization() is done");
			logger.debug("MysqlRepository.saveOrganization #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;
			
			
		} catch (Exception e) {			
			genericResponse.setMessage(e.getMessage());
			logger.info("MysqlRepository.saveOrganization : saveOrganization() is done");
			logger.debug("MysqlRepository.saveOrganization #genericResponse : {}", gsonTemplate.toJson(genericResponse));
			return genericResponse;			
		}
	}
	
	public Connection getMysqlConnection(AppInstance appInstance) {
		
		try {
			
			mysqlDataSource.setServerName("35.211.12.136");
			mysqlDataSource.setPortNumber(3306);
			mysqlDataSource.setDatabaseName("monetadb");
			mysqlDataSource.setUser("root");
			mysqlDataSource.setPassword("Lctr2020");			
			
//			mysqlDataSource.setServerName(appInstance.getI7072());
//			mysqlDataSource.setPortNumber(3306);
//			mysqlDataSource.setDatabaseName("monetadb");
//			mysqlDataSource.setUser(appInstance.getI7076());
//			mysqlDataSource.setPassword(appInstance.getI7077());
			
			Connection connection = mysqlDataSource.getConnection();
			
			logger.info("MysqlRepository.getMysqlConnection : getMysqlConnection() is done");
			logger.debug("MysqlRepository.getMysqlConnection #connection : {}", gsonTemplate.toJson(connection));
			return connection;
			
		}catch (Exception e) {
			logger.error("MysqlRepository.saveOrganization #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		
	}

}
