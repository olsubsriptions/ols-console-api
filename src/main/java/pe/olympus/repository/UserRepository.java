package pe.olympus.repository;

import java.util.List;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.UserDTO;
import pe.olympus.model.UserS;

public interface UserRepository {
	
	UserS getUserById(String userId);
	List<UserS> getUsersBySubscriptionId (String subscriptionId);
	GenericResponse loginValidation(UserDTO userDto);
	String getEmailValidationLink(String cid);
	UserS getUserInfo(UserDTO userDto);
	GenericResponse userUnlock(UserDTO userDto);
	GenericResponse userChangePassword(UserDTO userDto);	
}
