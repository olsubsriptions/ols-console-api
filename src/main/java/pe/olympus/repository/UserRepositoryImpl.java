package pe.olympus.repository;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.UpdateRequest;
import com.google.gson.Gson;

import pe.olympus.constants.Constants;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.constants.UserStatus;
import pe.olympus.model.CipherRequest;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.ResponseObject;
import pe.olympus.model.UserDTO;
import pe.olympus.model.UserS;
import pe.olympus.service.CryptoService;

@Repository
public class UserRepositoryImpl implements UserRepository{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private Firestore firebaseClient;
	private ObjectMapper mapper;
	private FirebaseApp firebaseApp;
	private Gson gsonTemplate;
	private CryptoService cryptoService;

	@Autowired
	public UserRepositoryImpl(Firestore firebaseClient,
			ObjectMapper mapper,
			FirebaseApp firebaseApp,
			Gson gsonTemplate,
			CryptoService cryptoService) {
		this.firebaseClient = firebaseClient;
		this.mapper = mapper;
		this.firebaseApp = firebaseApp;
		this.gsonTemplate = gsonTemplate;
		this.cryptoService = cryptoService;
	}

	@Override
	public UserS getUserById(String userId){
		
		try {
			
			DocumentSnapshot userDocument = firebaseClient.collection(Constants.T_USERS)
					.document(userId).get().get();
			
			UserS user = mapper.convertValue(userDocument.getData(), UserS.class);
			logger.debug("UserRepository.getUserById convertValue() #user : {}", gsonTemplate.toJson(user));
			
			if (user != null) {
				user.setG6731(userDocument.getId());
			}		
			
			logger.info("UserRepository.getUserById : getUserById() is done");
			logger.debug("UserRepository.getUserById #user : {}", gsonTemplate.toJson(user));
			return user;
			
		} catch (Exception e) {
			logger.error("UserRepository.getUserById #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public List<UserS> getUsersBySubscriptionId(String subscriptionId) {
		 
		try {
			
			List<UserS> listUsers = null;
			QuerySnapshot querySnapshotU = firebaseClient.collection(Constants.T_USERS)
					.whereEqualTo("b4321", subscriptionId).get().get();
			
			List<QueryDocumentSnapshot> userDocuments = querySnapshotU.getDocuments();
			
			if(!userDocuments.isEmpty()) {
				listUsers = new ArrayList<UserS>();				
				for(QueryDocumentSnapshot userDocument : userDocuments) {
					
					UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).getUser(userDocument.getId());
					UserS user = mapper.convertValue(userDocument.getData(), UserS.class);
					user.setG6731(userDocument.getId());
					user.setDisplayName(userRecord.getDisplayName());
					user.setEmail(userRecord.getEmail());
					user.setPhoneNumber(userRecord.getPhoneNumber());					
					logger.debug("UserRepository.getUsersBySubscriptionId convertValue() #user : {}", gsonTemplate.toJson(user));
					
					listUsers.add(user);
				}
			}
			
			logger.info("UserRepository.getUsersBySubscriptionId : getUsersBySubscriptionId() is done");
			logger.debug("UserRepository.getUsersBySubscriptionId #listUsers : {}", gsonTemplate.toJson(listUsers));
			return listUsers;
			
		} catch (Exception e) {
			logger.error("UserRepository.getUsersBySubscriptionId #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public String getEmailValidationLink(String cid) {
		
		try {
			QuerySnapshot querySnapshotU = firebaseClient.collection(Constants.T_USERS)
					.whereEqualTo("g6734", cid).get().get();
			
			DocumentSnapshot userDocument = querySnapshotU.getDocuments().get(0);
			
			DocumentSnapshot suscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.document(userDocument.getString("b4321")).get().get();
			
			DocumentSnapshot organizationDocument = firebaseClient.collection(Constants.T_ORGANIZATIONS)
					.document(suscriptionDocument.getString("f4461")).get().get();
					
//			URI uri = new URI(organizationDocument.getString("f4477"));
//			System.setProperty("java.awt.headless", "false");
//			java.awt.Desktop.getDesktop().browse(uri);
			
			logger.info("UserRepository.getEmailValidationLink : getEmailValidationLink() is done");
			
			return organizationDocument.getString("f4477");
			
		} catch (Exception e) {
			logger.error("UserRepository.getEmailValidationLink #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse loginValidation(UserDTO userDto) {
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.EMAIL_NOTVERIFIED);
			
			String userId = userDto.getUsername().split("@")[0];
			
			DocumentSnapshot userDocument = firebaseClient.collection(Constants.T_USERS)
					.document(userId).get().get();
			
			if(userDocument.getLong("g6735") == UserStatus.ACTIVE) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.EMAIL_VERIFIED);
			} else {
				UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).getUser(userDocument.getId());
				if(userRecord.isEmailVerified()) {
					DocumentReference userReference = firebaseClient.collection(Constants.T_USERS)
							.document(userId);
					WriteResult userWr = userReference.update("g6735", UserStatus.ACTIVE).get();
					
					DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
							.document(userDocument.getString("b4321")).get().get();
					
					DocumentReference organizationReference = firebaseClient.collection(Constants.T_ORGANIZATIONS)
							.document(subscriptionDocument.getString("f4461"));
					
					WriteResult organizationWr = organizationReference.update("f4476", true).get();
					
					if(userWr != null && organizationWr != null) {
						genericResponse.setStatus(true);
						genericResponse.setMessage(ResponseMessages.EMAIL_VERIFIED);
					}
				}
			}			
			logger.info("UserRepository.loginValidation : loginValidation() is done");
			return genericResponse;
		} catch (Exception e) {
			logger.error("UserRepository.loginValidation #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		
	}

	@Override
	public UserS getUserInfo(UserDTO userDTO) {
		UserS user = null;
		String domain = userDTO.getUsername().split("@")[1];
		try {
			QuerySnapshot querySnapshotU = firebaseClient.collection(Constants.T_USERS)
					.whereEqualTo("g6733", cryptoService.hash384(domain.toLowerCase()))
					.get().get();
			if(!querySnapshotU.isEmpty()) {
				DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
						.document(querySnapshotU.getDocuments().get(0).getString("b4321"))
						.get().get();
						
				if(subscriptionDocument.exists()) {
					String indexKey = subscriptionDocument.getString("b4325");
					String cipherData = userDTO.getUsername().toLowerCase();
					// String key = subscriptionDocument.getString("b4322");
					
					CipherRequest cipherRequest = cryptoService.getDefaultCipherRequest(cipherData, indexKey);
					
					ResponseObject response = cryptoService.encrypt(cipherRequest);
					
					if(response.getDataList().size() > 0) {
						
						QuerySnapshot querySnapshotU2 = firebaseClient.collection(Constants.T_USERS)
								.whereEqualTo("g6732", response.getDataList().get(0))
								.get().get();
						if(!querySnapshotU2.isEmpty()) {
							DocumentSnapshot userDocument = querySnapshotU2.getDocuments().get(0);
							
							DocumentSnapshot organizationDocument = firebaseClient.collection(Constants.T_ORGANIZATIONS)
									.document(subscriptionDocument.getString("f4461"))
									.get().get();
							if(organizationDocument.exists()) {
								user = new UserS();
								user.setG6731(userDocument.getId());
								user.setG6732(userDocument.getString("g6732"));
								user.setG6733(userDocument.getString("g6733"));
								
								cipherRequest = cryptoService.getDefaultCipherRequest(organizationDocument.getString("f4469"), indexKey);								
								response = cryptoService.decrypt(cipherRequest);
								
								user.setPhoneNumber(response.getDataList().get(0));
								
								cipherRequest = cryptoService.getDefaultCipherRequest(organizationDocument.getString("f4471"), indexKey);								
								response = cryptoService.decrypt(cipherRequest);
								
								user.setEmail(response.getDataList().get(0));
							}							
						}
					}	
				}
						
			}
			logger.info("UserRepository.getUserInfo : getUserInfo() is done");
			return user;
		} catch (Exception e) {
			logger.error("UserRepository.getUserInfo #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse userUnlock(UserDTO userDto) {		
		
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.USER_NOTFOUND);
			
			String userId = userDto.getUsername();
			UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).getUser(userId);
			if(!userRecord.equals(null)) {
				genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
				UpdateRequest userUpdateRequest = userRecord.updateRequest().setDisabled(false);
				UserRecord updatedUserRecord = FirebaseAuth.getInstance(firebaseApp).updateUser(userUpdateRequest);
				if(!updatedUserRecord.isDisabled()) {
					genericResponse.setStatus(true);
					genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
				}
			}
			logger.info("UserRepository.userUnlock : userUnlock() is done");
			return genericResponse;
		} catch (Exception e) {			
			logger.error("UserRepository.userUnlock #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}

	@Override
	public GenericResponse userChangePassword(UserDTO userDto) {
		try {
			GenericResponse genericResponse = new GenericResponse();
			genericResponse.setStatus(false);
			genericResponse.setMessage(ResponseMessages.USER_NOTFOUND);
			
			String userId = userDto.getUsername();
			UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).getUser(userId);
			if(!userRecord.equals(null)) {
				genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
				UpdateRequest userUpdateRequest = userRecord.updateRequest().setPassword(userDto.getPassword());
				UserRecord updatedUserRecord = FirebaseAuth.getInstance(firebaseApp).updateUser(userUpdateRequest);
				if(!updatedUserRecord.isDisabled()) {
					genericResponse.setStatus(true);
					genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
				}
			}
			logger.info("UserRepository.userChangePassword : userChangePassword() is done");
			return genericResponse;
		} catch (Exception e) {			
			logger.error("UserRepository.userChangePassword #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
}
