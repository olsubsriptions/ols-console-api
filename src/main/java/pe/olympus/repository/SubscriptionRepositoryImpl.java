package pe.olympus.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.gson.Gson;

import pe.olympus.constants.Constants;
import pe.olympus.model.SubsConfig;
import pe.olympus.model.Subscription;

@Repository
public class SubscriptionRepositoryImpl implements SubscriptionRepository{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private Firestore firebaseClient;
	private ObjectMapper mapper;
	private Gson gsonTemplate;

	@Autowired
	public SubscriptionRepositoryImpl(Firestore firebaseClient,
			ObjectMapper mapper,
			Gson gsonTemplate) {
		this.firebaseClient = firebaseClient;
		this.mapper = mapper;
		this.gsonTemplate = gsonTemplate;
	}
	
	@Override
	public Subscription getSubscriptionById(String subscriptionId){

		try {
			
			DocumentSnapshot subscriptionDocument = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.document(subscriptionId).get().get();
			
			Subscription subscription = mapper.convertValue(subscriptionDocument.getData(), Subscription.class);
			
			if (subscription != null) {
				subscription.setB4321(subscriptionDocument.getId());
			}	
			logger.debug("SubscriptionRepository.getSubscriptionById convertValue() #subscription : {}", gsonTemplate.toJson(subscription));
			
			logger.info("SubscriptionRepository.getSubscriptionById : getSubscriptionById() is done");
			logger.debug("SubscriptionRepository.getSubscriptionById #subscription : {}", gsonTemplate.toJson(subscription));
			return subscription;
			
		} catch (Exception e) {
			
			logger.error("SubscriptionRepository.getSubscriptionById #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public Subscription updateSubscriptionDerivativeKey(String subscriptionId, String derivativeKey){
		
		try {
			
			Subscription subscription = null;
			DocumentReference subscriptionReference = firebaseClient.collection(Constants.T_SUBSCRIPTIONS)
					.document(subscriptionId);
			WriteResult writeResult = subscriptionReference.update("b4324", derivativeKey).get();			
			
			if(writeResult != null) {
				
				DocumentSnapshot subscriptionDocument = subscriptionReference.get().get();				
				subscription = mapper.convertValue(subscriptionDocument.getData(), Subscription.class);			
				subscription.setB4321(subscriptionDocument.getId());
				logger.debug("SubscriptionRepository.updateSubscriptionDerivativeKey convertValue() #subscription : {}", gsonTemplate.toJson(subscription));
			}
			
			logger.info("SubscriptionRepository.updateSubscriptionDerivativeKey : updateSubscriptionDerivativeKey() is done");
			logger.debug("SubscriptionRepository.updateSubscriptionDerivativeKey #subscription : {}", gsonTemplate.toJson(subscription));
			return subscription;
			
		} catch (Exception e) {
			logger.error("SubscriptionRepository.updateSubscriptionDerivativeKey #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}

	@Override
	public SubsConfig getConfiguration() {
		try {
			SubsConfig configuration = null;
			DocumentSnapshot configurationDocument = firebaseClient.collection(Constants.T_CONFIG)
					.document("cnfg01")
					.get().get();
			if(configurationDocument.exists()) {
				configuration = new SubsConfig();
				configuration.setServicePort(configurationDocument.getString("servicePort"));
				configuration.setSubscriptionEndPoint(configurationDocument.getString("subscriptionEndpoint"));
			}			
			
			logger.info("SubscriptionRepository.getConfiguration : getConfiguration() is done");
			logger.debug("SubscriptionRepository.getConfiguration #configuration : {}", gsonTemplate.toJson(configuration));
			return configuration;
		} catch (Exception e) {
			logger.error("SubscriptionRepository.getConfiguration #error : {}", e.getMessage());
			throw new RuntimeException(e.getMessage());
		}		
	}	
	
}
