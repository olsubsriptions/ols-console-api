package pe.olympus.repository;

import pe.olympus.model.Organization;

public interface OrganizationRepository {
	
	Organization getOrganizationBySubscriptionId(String subscriptionId);

}
