package pe.olympus.repository;

import pe.olympus.model.AppInstance;
import pe.olympus.model.GenericResponse;

public interface MysqlRepository {

	public GenericResponse saveOrganization(AppInstance appInstance, String indexKey, String organizationName, String cola);
}
