package pe.olympus.controller.command;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.GenericResponse;
import pe.olympus.model.UserDTO;
import pe.olympus.model.UserS;
import pe.olympus.service.UserService;

@RestController
@RequestMapping("/users")
public class UserControllerCommand {
	
	private UserService userService;
	
	@Autowired
	public UserControllerCommand(UserService userService) {
		
		this.userService = userService;
	}
	
	@PostMapping("/login/validation")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> loginValidation(@RequestBody UserDTO userDto) {
		
		GenericResponse genericResponse = userService.loginValidation(userDto);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping("/info")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<UserS> userInfo(@RequestBody UserDTO userDto){
		UserS user = userService.getUserInfo(userDto);
		return new ResponseEntity<UserS>(user, HttpStatus.OK);
	}
	
	@PostMapping("/info/unlock")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> userUnlock(@RequestBody UserDTO userDto){
		GenericResponse genericResponse = userService.userUnlock(userDto);
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}	
	@PostMapping("/info/changePass")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> userChangePassword(@RequestBody UserDTO userDto){
		GenericResponse genericResponse = userService.userChangePassword(userDto);
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
}
