package pe.olympus.controller.command;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.AppInstance;
import pe.olympus.model.Application;
import pe.olympus.model.GenericResponse;
import pe.olympus.service.ApplicationService;

@RestController
@RequestMapping("/applications")
public class ApplicationControllerCommand {

	private ApplicationService applicationService;
	
	@Autowired
	public ApplicationControllerCommand(ApplicationService applicationService) {
		
		this.applicationService = applicationService;
	}
	
	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> createApplication(@RequestBody @Valid Application application){	
		
		GenericResponse genericResponse = applicationService.createApplication(application);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping("/instances")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> createInstance(@RequestBody @Valid AppInstance appInstance){	
		
		GenericResponse genericResponse = applicationService.createInstance(appInstance);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping("/instances/{instanceId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> updateInstance(@RequestBody @Valid AppInstance appInstance){	
		
		GenericResponse genericResponse = applicationService.updateInstance(appInstance);
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
}
