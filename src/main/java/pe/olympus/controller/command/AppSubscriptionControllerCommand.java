package pe.olympus.controller.command;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.BindDTO;
import pe.olympus.model.AgentDTO;
import pe.olympus.model.AppSubsComponent;
import pe.olympus.model.AppSubscription;
import pe.olympus.model.GenericResponse;
import pe.olympus.model.VerifyResponse;
import pe.olympus.service.AppSubscriptionService;

@RestController
@RequestMapping("/applications/subscriptions")
public class AppSubscriptionControllerCommand {
	
	private AppSubscriptionService appSubscriptionService;
	
	@Autowired
	public AppSubscriptionControllerCommand(AppSubscriptionService appSubscriptionService) {
		
		this.appSubscriptionService = appSubscriptionService;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> createAppSubscription(@RequestBody @Valid AppSubscription appSubscription) {
		
		GenericResponse genericResponse = appSubscriptionService.createAppSubscription(appSubscription);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping ("/{appSubscriptionId}/components")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> createAppSubsComponents(@RequestBody @Valid List<AppSubsComponent> listAppSubsComponent) {
		
		GenericResponse genericResponse = appSubscriptionService.createAppSubsComponents(listAppSubsComponent);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@PostMapping("/bind")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<VerifyResponse> verifySubscription(@RequestBody @Valid BindDTO bindDTO) {
		
		VerifyResponse verifyResponse = appSubscriptionService.verifySubscription(bindDTO);			
		return new ResponseEntity<VerifyResponse>(verifyResponse, HttpStatus.OK);
		
	}
	
	@PostMapping("/data")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> saveApplicationData(@RequestBody @Valid AgentDTO agentDTO) {
		
		GenericResponse genericResponse = appSubscriptionService.saveApplicationData(agentDTO);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
		
	}
	
	@PostMapping("/pagape")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> createAppSubscriptionPagape(@RequestBody @Valid AppSubscription appSubscription) {
		
		GenericResponse genericResponse = appSubscriptionService.createAppSubscription(appSubscription);			
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
}
