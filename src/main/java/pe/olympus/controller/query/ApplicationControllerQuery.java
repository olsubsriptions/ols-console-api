package pe.olympus.controller.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.AppComponent;
import pe.olympus.model.Application;
import pe.olympus.model.ApplicationDTO;
import pe.olympus.service.ApplicationService;

@RestController
@RequestMapping("/applications")
public class ApplicationControllerQuery {

	private ApplicationService applicationService;
	
	@Autowired
	public ApplicationControllerQuery(ApplicationService applicationService) {
		
		this.applicationService = applicationService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<Application>> getApplications(@RequestParam(value="userId", required= true) String userId){
		
		List<Application> listApplication = applicationService.getApplications(userId);			
		return new ResponseEntity<List<Application>>(listApplication, HttpStatus.OK);			
	}
	
	@GetMapping("/all")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<ApplicationDTO>> getAllApplications() {
		
		List<ApplicationDTO> listApplication = applicationService.getAllApplications();
		return new ResponseEntity<List<ApplicationDTO>>(listApplication, HttpStatus.OK);
	}
	
	@GetMapping ("/components")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<AppComponent>> getAppComponentsByAppSubscriptionId(@RequestParam(value="appSubscriptionId", required= true) String appSubscriptionId,
			@RequestParam(value="appShortName", required= true) String appShortName){
		
		List<AppComponent> listAppComponent = applicationService.getAppComponentsByAppSubscriptionId(appSubscriptionId, appShortName);
		return new ResponseEntity<List<AppComponent>>(listAppComponent, HttpStatus.OK);
	}
}
