package pe.olympus.controller.query;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import com.google.firebase.auth.UserRecord.CreateRequest;
import com.google.firebase.auth.UserRecord.UpdateRequest;
import com.google.gson.Gson;

import pe.olympus.constants.Constants;
import pe.olympus.constants.ResponseMessages;
import pe.olympus.model.BackupObject;
import pe.olympus.model.GenericResponse;

@RestController
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	private Firestore firebaseClient;
	@Autowired
	private Gson gsonTemplate;
	@Autowired
	private FirebaseApp firebaseApp;

	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public String home() {
		return "Ols Console Api STARTED";
	}
	
	@GetMapping ("/uid")
	@ResponseStatus(HttpStatus.OK)
	public String uid() {
		return UUID.randomUUID().toString();
	}
	
	@GetMapping ("/link")
	@ResponseStatus(HttpStatus.OK)
	public String link(@RequestParam(value = "uid", required= true) String uid ) {
		
		try {
//			UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).getUser(uid);
//			System.out.print(userRecord.isEmailVerified());
//			UpdateRequest a = userRecord.updateRequest().setEmailVerified(false);
//			userRecord = FirebaseAuth.getInstance().updateUser(a);
			
			return FirebaseAuth.getInstance(firebaseApp).generateEmailVerificationLink(uid + "@olympus-cloud.com");
		} catch (FirebaseAuthException e) {
			
			e.printStackTrace();
			return "error getting link";
		}
	}
	
	@GetMapping ("/backup")
	@ResponseStatus(HttpStatus.OK)
	public List<BackupObject> backup(@RequestParam(value = "table", required= true) String table) throws InterruptedException, ExecutionException {
		QuerySnapshot querySnap = firebaseClient.collection(table)
				.get().get();
		
		List<QueryDocumentSnapshot> docs = querySnap.getDocuments();
		
		List<BackupObject> docsBack = new ArrayList<BackupObject>();
		
		for(QueryDocumentSnapshot doc : docs) {
			BackupObject temp = new BackupObject();
			temp.setTable(table);
			temp.setId(doc.getId());
			temp.setData(doc.getData());
			docsBack.add(temp);
		}
		
		return docsBack;
	}
	
	@PostMapping ("/restore")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> restore(@RequestBody List<BackupObject> listBackupObject) throws InterruptedException, ExecutionException{
		GenericResponse genericResponse = new GenericResponse();
		
		for(BackupObject backupObject: listBackupObject) {
			DocumentReference documentRef = firebaseClient.collection(backupObject.getTable())
					.document(backupObject.getId());			
			
			WriteResult writeResult = documentRef.create(backupObject.getData()).get();
			
			if(writeResult != null) {
				genericResponse.setStatus(true);
				genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
			}
			else {
				genericResponse.setStatus(false);
				genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
				new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK); 
			}
		}		
		
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}
	
	@GetMapping ("/createUser")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<GenericResponse> createUser() throws FirebaseAuthException{
		
		GenericResponse genericResponse = new GenericResponse();
		genericResponse.setStatus(false);
		genericResponse.setMessage(ResponseMessages.OPERACION_NOEXITOSA);
		
		String userUID = "82f0b3e4-bc33-4fe9-ba75-a9a7f92fa7c1";
		String password = "Lctr2020";
		String userName = "test@testcorp";
		String phoneNumber = "+51957918467";
		
		CreateRequest createUserRequest = new CreateRequest();
		createUserRequest.setUid(userUID);
		createUserRequest.setEmail(userUID + "@olympus-cloud.com");
		createUserRequest.setEmailVerified(true);
		createUserRequest.setPassword(password);
		createUserRequest.setDisplayName(userName);
		createUserRequest.setDisabled(false);
		createUserRequest.setPhoneNumber(phoneNumber);
		
		UserRecord userRecord = FirebaseAuth.getInstance(firebaseApp).createUser(createUserRequest);
		if(userRecord != null) {				
			genericResponse.setStatus(true);
			genericResponse.setMessage(ResponseMessages.OPERACION_EXITOSA);
		}
		
		return new ResponseEntity<GenericResponse>(genericResponse, HttpStatus.OK);
	}

}
