package pe.olympus.controller.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.UserS;
import pe.olympus.service.UserService;

@RestController
public class UserControllerQuery {

private UserService userService;
	
	@Autowired
	public UserControllerQuery(UserService userService) {
		this.userService = userService;
	}
	
	@GetMapping("/users")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<UserS>> getUsers(@RequestParam(value="userId", required= true) String userId) {
		
		List<UserS> listUsers = userService.getUsers(userId);
		return new ResponseEntity<>(listUsers, HttpStatus.OK);
	}
	
//	@GetMapping("/v")
//	@ResponseStatus(HttpStatus.OK)
//	public void emailValidation(@RequestParam(value="cid", required= true) String cid) {		
//		userService.emailValidation(cid);
//	}
}
