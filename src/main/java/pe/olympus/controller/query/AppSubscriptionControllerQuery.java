package pe.olympus.controller.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.AppSubscription;
import pe.olympus.model.ResponseObject;
import pe.olympus.model.VerifyResponse;
import pe.olympus.service.AppSubscriptionService;

@RestController
@RequestMapping("/applications/subscriptions")
public class AppSubscriptionControllerQuery {

	private AppSubscriptionService appSubscriptionService;
	
	@Autowired
	public AppSubscriptionControllerQuery(AppSubscriptionService appSubscriptionService) {
		
		this.appSubscriptionService = appSubscriptionService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<AppSubscription>> getAppsSubscriptionByUserId(@RequestParam(value="userId", required= true) String userId) {
		
		List<AppSubscription> listAppSubscription = appSubscriptionService.getAppsSubscriptionByUserId(userId);			
		return new ResponseEntity<List<AppSubscription>>(listAppSubscription, HttpStatus.OK);
	}
	
	@GetMapping ("/{appSubscriptionId}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<AppSubscription> getAppSubscription(@PathVariable(value = "appSubscriptionId") String appSubscriptionId){
		
		AppSubscription appSubscription = appSubscriptionService.getAppSubscriptionById(appSubscriptionId);
		return new ResponseEntity<AppSubscription>(appSubscription, HttpStatus.OK);
	}
	
	@GetMapping ("/k2")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseObject> getK2(@RequestParam(value = "appSubscriptionId", required= true) String appSubscriptionId) {
		
		ResponseObject response = appSubscriptionService.getK2(appSubscriptionId);
		return new ResponseEntity<ResponseObject>(response, HttpStatus.OK);
			
	}
	
	@GetMapping ("/regeneratek2")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseObject> regenerateK2(@RequestParam(value = "appSubscriptionId", required= true) String appSubscriptionId) {
		
		ResponseObject response = appSubscriptionService.regenerateK2(appSubscriptionId);
		return new ResponseEntity<ResponseObject>(response, HttpStatus.OK);
	}
	
	@GetMapping ("/verify")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<VerifyResponse> verifySubscriptionByIndexKey (@RequestParam(value="indexKey", required = true) String indexKey,
			@RequestParam (value = "workingApplicationKey", required = true) String workingApplicationKey) {
		
		VerifyResponse verifyResponse = appSubscriptionService.verifySubscriptionByIndexKey(indexKey, workingApplicationKey);
		return new ResponseEntity<VerifyResponse>(verifyResponse, HttpStatus.OK);
	}
}
