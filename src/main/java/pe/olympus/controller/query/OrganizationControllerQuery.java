package pe.olympus.controller.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.Organization;
import pe.olympus.service.OrganizationService;

@RestController
@RequestMapping("/organization")
public class OrganizationControllerQuery {

	private OrganizationService organizationService;
	
	@Autowired
	public OrganizationControllerQuery(OrganizationService organizationService) {
		
		this.organizationService = organizationService;
	}
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Organization> getOrganizationByUserId(@RequestParam(value="userId", required= true) String userId) {
		
		Organization organization = organizationService.getOrganizationByUserId(userId);			
		return new ResponseEntity<Organization>(organization, HttpStatus.OK);
	}

}
