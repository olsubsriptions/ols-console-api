package pe.olympus.controller.query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pe.olympus.model.ResponseObject;
import pe.olympus.model.Subscription;
import pe.olympus.service.SubscriptionService;

@RestController
@RequestMapping("/subscriptions")
public class SubscriptionControllerQuery {
	
	private SubscriptionService subscriptionService;
	
	@Autowired
	public SubscriptionControllerQuery(SubscriptionService subscriptionService) {
		
		this.subscriptionService = subscriptionService;
	}
	
	@GetMapping
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Subscription> getSubscriptionByUserId(@RequestParam(value="userId", required= true) String userId){
		
		Subscription subscription = subscriptionService.getSubscriptionByUserId(userId);			
		return new ResponseEntity<Subscription>(subscription, HttpStatus.OK);			
	}
	
	@GetMapping ("/k1")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseObject> getK1(@RequestParam(value="userId", required= true) String userId) {
		
		ResponseObject response = subscriptionService.getK1(userId);
		return new ResponseEntity<ResponseObject>(response, HttpStatus.OK);			
	}
	
	@GetMapping ("/regeneratek1")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<ResponseObject> regenerateK1(@RequestParam(value="userId", required= true) String userId) {
		
		ResponseObject response = subscriptionService.regenerateK1(userId);
		return new ResponseEntity<ResponseObject>(response, HttpStatus.OK);				
	}
}
