package pe.olympus.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PropertiesConfig {
	
	@Value("${cryptosvc.url}")
	private String cryptosvcUrl;

	public String getCryptosvcUrl() {
		return cryptosvcUrl;
	}

	public void setCryptosvcUrl(String cryptosvcUrl) {
		this.cryptosvcUrl = cryptosvcUrl;
	}
	
}
