package pe.olympus.config;

import java.io.IOException;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.gson.Gson;
import com.mysql.cj.jdbc.MysqlDataSource;

@Configuration
public class OlsconsoleConfiguration {
	
	@Bean
	public Firestore firebaseClient() throws IOException {
		FirestoreOptions firestoreOptions =
				FirestoreOptions.getDefaultInstance().toBuilder()
				.setCredentials(GoogleCredentials.getApplicationDefault())
				.setProjectId("subcriptions-2020-02")
				.build();
		Firestore fireStore = firestoreOptions.getService();
		return fireStore;		
	}
	
	@Bean 
	public FirebaseApp firebaseApp() throws Exception {
		
		FirebaseApp firebaseApp = null;
		
		FirebaseOptions options = new FirebaseOptions.Builder() 
			    .setCredentials(GoogleCredentials.getApplicationDefault())
			    .setProjectId("subcriptions-2020-02")
			    .build();
		 
		boolean hasBeenInitialized = false;
		List<FirebaseApp> firebaseApps = FirebaseApp.getApps();
		for(FirebaseApp app : firebaseApps){
		    if(app.getName().equals(FirebaseApp.DEFAULT_APP_NAME)){
		        hasBeenInitialized = true;
		        firebaseApp = app;
		    }
		}

		if(!hasBeenInitialized) {
		    firebaseApp = FirebaseApp.initializeApp(options);
		}
		
		return firebaseApp;
	}
		
	@Bean
	public MysqlDataSource  mysqlDataSource() {
		MysqlDataSource mysqlDataSource = new MysqlDataSource();
		return mysqlDataSource;
	}
	
	@Bean
	public ObjectMapper mapper() {		
		return new ObjectMapper();		
	}
	
	@Bean
	public RestTemplate rest() {
		return new RestTemplate();
	}
	
	@Bean
	public Gson gsonTemplate() {
		return new Gson();
	}
}
